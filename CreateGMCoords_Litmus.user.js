// ==UserScript==
// @name         CreateGMCoords_Litmus
// @namespace    http://tampermonkey.net/
// @version      2.0
// @description  Samla ihop cache koordinater vid Litmus Test
// @updateURL    https://gitlab.com/herrFamaxe/src/raw/main/CreateGMCoords_Litmus.user.js
// @downloadURL  https://gitlab.com/herrFamaxe/src/raw/main/CreateGMCoords_Litmus.user.js
// @author       Mats
// @match        https://admin.geocaching.com/LitmusTest/*
// @icon         data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw==
// @grant        GM_registerMenuCommand
// @grant        GM_getValue
// @grant        GM_setValue
// ==/UserScript==

        //Tar en tidsstämpel
        var d1 = new Date();
        var n1 = d1.getTime();

        var iRadie = GM_getValue('Where_Radie')
        if (iRadie === undefined) {
            iRadie=161;
        }
        var hText='';
        var sArray = document.getElementsByTagName("script");
        var x1, x2, x3;
        var y1, y2, y3;
        var sJSON='';
        var aJSONarray=[];

        // Sätter rubrik = cachenamn
        var pTitle = document.title;
        pTitle=pTitle.replace('- Reviewer Litmus Test' ,'');

        // letar rätt på window.LitmusTest.jsonData som innehåller all cache-info
        for (x1=0; x1 < sArray.length; x1++) {
            if (sArray[x1].innerHTML.indexOf('window.LitmusTest.jsonData = {') > 0){
                x2 = sArray[x1].innerHTML.indexOf('window.LitmusTest.jsonData = {');
                x3 = sArray[x1].innerHTML.indexOf('};', x2);
                sJSON = sArray[x1].innerHTML.substring(x2,x3+4);
                x1 = 99999;
            }
        }
        // delar upp window.LitmusTest.jsonData i 4+1 arrayer, 4 olika type + 1 med ytterligare info kring otherCaches och otherWaypoints
        x2 = sJSON.indexOf('"otherCaches":{',0);
        x3 = sJSON.indexOf('"otherWaypoints":{', x2);
        var sJSON1 = sJSON.substring(x2,x3);

        x2 = sJSON.indexOf('"otherWaypoints":{',0);
        x3 = sJSON.indexOf('"ourCache":{', x2);
        var sJSON2 = sJSON.substring(x2,x3);

        x2 = sJSON.indexOf('"ourCache":{',0);
        x3 = sJSON.indexOf('"ourWaypoints":[', x2);
        var sJSON3 = sJSON.substring(x2,x3+1);

        x2 = sJSON.indexOf('"ourWaypoints":[',0);
        x3 = sJSON.indexOf('"resultItems":[', x2);
        var sJSON4 = sJSON.substring(x2,x3);

        x2 = sJSON.indexOf('"resultItems":[',0);
        x3 = sJSON.indexOf(']};', x2);
        var sJSON5 = sJSON.substring(x2,x3);
        var aJSONarray5 = sJSON5.split('},')

        var sKoord;
        var sGCKode;
        var sID;
        var sTyp;
        var sFind;
        var sVirt;
        var sRadius;
        var bRad='';
        var sColor1;
        var sColor2;
        var xTra;
       // kollar igenom alla fyra arrayer och samlar infon
       //otherCaches tar inte med arkiverade cacher --// OCH heller inte Disabled Unpublished // MEN Enalbled Disabled Unpublished komme med
        bRad='';
        aJSONarray = sJSON1.split('},')
        for (x1=0; x1 < aJSONarray.length; x1++) {
            aJSONarray[x1] += '}'; // fuling för att avsluta raderna rätt  // && aJSONarray[x1].indexOf('Disabled Unpublished') == -1
            if (aJSONarray[x1].indexOf('approvedByID') > 0 && aJSONarray[x1].indexOf('Archived Disabled') == -1 ) {
                x2=aJSONarray[x1].indexOf('"latLng":[', 0);
                x3=aJSONarray[x1].indexOf(']', x2+10);
                sKoord = aJSONarray[x1].substring(x2+10,x3);
                x2=aJSONarray[x1].indexOf('"gcCode":"', 0);
                x3=aJSONarray[x1].indexOf('"', x2+10);
                sGCKode = aJSONarray[x1].substring(x2+10,x3);
                x2=aJSONarray[x1].indexOf('"id":', 0);
                x3=aJSONarray[x1].indexOf(',', x2+5);
                sID = aJSONarray[x1].substring(x2+5,x3);
                x2=aJSONarray[x1].indexOf('"typeID":', 0);
                x3=aJSONarray[x1].indexOf('}', x2+9);
                sTyp = aJSONarray[x1].substring(x2+9, x3);

                xTra='';
                sColor1=', red';
                sColor2=', gray';
                if (aJSONarray[x1].indexOf('Disabled Unpublished') > 0 ) {
                   // alert(aJSONarray[x1]);
                   xTra='/d';
                   sColor1=', white';
                   sColor2=', white';
              }

                sVirt='';
                sFind='otherCacheID":' + sID + ',';
                for (y1=0; y1 < aJSONarray5.length; y1++) { // kolla i array 5 om cachen är virtuell
                    if (aJSONarray5[y1].indexOf(sFind) != -1) {
                        if (aJSONarray5[y1].indexOf('"Listing Is Virtual"') != -1) {
                            sVirt='-V';
                            y1=999999
                        }
                    }
                }
                sRadius = 'radius=' + iRadie + sColor1;
                if (sTyp==2) {
                   sGCKode = sGCKode + '-tr';
                } else if (sTyp==3) {
                    sGCKode = sGCKode + '-mu';
                    if(sVirt!='') {
                        sGCKode = sGCKode + 'V';
                        sRadius = 'radius=' + iRadie + sColor2;
                    }
                } else if (sTyp==5) {
                    sGCKode = sGCKode + '-lb';
                    if(sVirt!='') {
                        sGCKode = sGCKode + 'V';
                        sRadius = 'radius=' + iRadie + sColor2;
                    }
                } else if (sTyp==8) {
                    sGCKode = sGCKode + '-my';
                    if(sVirt!='') {
                        sGCKode = sGCKode + 'V';
                        sRadius = 'radius=' + iRadie + sColor2;
                    }
                } else if (sTyp==1858) {
                    sGCKode = sGCKode + '-wg';
                    if(sVirt!='') {
                        sGCKode = sGCKode + 'V';
                        sRadius = 'radius=' + iRadie + sColor2;
                    }
                } else {
                   sGCKode = sGCKode + '-?';
                }

                bRad = sKoord + ' <' + sGCKode + xTra + '>\r';
                bRad = bRad + sRadius
                hText = hText + bRad + '\r';
            }
        }

        //otherWaypoints tar inte med wp till arkiverade cacher OCH heller inte Unpublished
        bRad='';
        aJSONarray = sJSON2.split('},')
        for (x1=0; x1 < aJSONarray.length; x1++) {
            aJSONarray[x1] += '}'; // fuling för att avsluta raderna rätt
            if (aJSONarray[x1].indexOf('"parentCacheIsArchived"') > 0 && aJSONarray[x1].indexOf('"parentCacheIsArchived":true') == -1 && aJSONarray[x1].indexOf('"parentCacheCSS":"Disabled Unpublished"') == -1) {
                x2=aJSONarray[x1].indexOf('"latLng":[', 0);
                x3=aJSONarray[x1].indexOf(']', x2+10);
                sKoord = aJSONarray[x1].substring(x2+10,x3);
                x2=aJSONarray[x1].indexOf('"parentCacheGCCode":"', 0);
                x3=aJSONarray[x1].indexOf('"', x2+21);
                sGCKode = aJSONarray[x1].substring(x2+21,x3);
                x2=aJSONarray[x1].indexOf('"id":', 0);
                x3=aJSONarray[x1].indexOf(',', x2+5);
                sID = aJSONarray[x1].substring(x2+5,x3);
                x2=aJSONarray[x1].indexOf('"typeID":', 0);
                x3=aJSONarray[x1].indexOf('}', x2+9);
                sTyp = aJSONarray[x1].substring(x2+9, x3);

                sRadius = 'radius=' + iRadie + ', red';
                if (sTyp==219) {
                    sGCKode = sGCKode + '-ws';
                } else if (sTyp==220) {
                    sGCKode = sGCKode + '-wf';
                } else {
                    sGCKode = sGCKode + '-w' + sTyp;
                }
                bRad = sKoord + ' <' + sGCKode + '>\r';
                bRad = bRad + sRadius;
                hText = hText + bRad + '\r';
            }
        }

        //ourCache
        x2=sJSON3.indexOf('"latLng":[', 0);
        x3=sJSON3.indexOf(']', x2+10);
        sKoord = sJSON3.substring(x2+10,x3);
        x2=sJSON3.indexOf('"gcCode":"', 0);
        x3=sJSON3.indexOf('"', x2+10);
        sGCKode = sJSON3.substring(x2+10,x3);
        x2=sJSON3.indexOf('"typeID":', 0);
        x3=sJSON3.indexOf('}', x2+9);
        sTyp = sJSON3.substring(x2+9, x3);

       sRadius = 'radius=' + iRadie + ', orange';
//2 trad
//3 multi
//5 letterbob
//8 mystery
//1858 wherigo
        if (sTyp==2) {
           sGCKode = sGCKode + '-tr/E';
         } else if (sTyp==3) {
           sGCKode = sGCKode + '-mu/E';
         } else if (sTyp==5) {
           sGCKode = sGCKode + '-lb/E';
         } else if (sTyp==8) {
           sGCKode = sGCKode + '-my/E';
         } else if (sTyp==1858) {
           sGCKode = sGCKode + '-wg/E';
         } else {
           sGCKode = sGCKode + '-?';
         }

         bRad = sKoord + ' <' + sGCKode + '>\r';
         bRad = bRad + sRadius
         hText = hText + bRad + '\r';

        //ourWaypoints
        bRad='';
        aJSONarray = sJSON4.split('},')
        for (x1=0; x1 < aJSONarray.length; x1++) {
            aJSONarray[x1] += '}'; // fuling för att avsluta raderna rätt
            if (aJSONarray[x1].indexOf('guid') > 0) { // && aJSONarray[x1].indexOf('Archived Disabled') == -1) {
                x2=aJSONarray[x1].indexOf('"latLng":[', 0);
                x3=aJSONarray[x1].indexOf(']', x2+10);
                sKoord = aJSONarray[x1].substring(x2+10,x3);
                x2=aJSONarray[x1].indexOf('"parentCacheGCCode":"', 0);
                x3=aJSONarray[x1].indexOf('"', x2+21);
                sGCKode = aJSONarray[x1].substring(x2+21,x3);
                x2=aJSONarray[x1].indexOf('"typeID":', 0);
                x3=aJSONarray[x1].indexOf('}', x2+9);
                sTyp = aJSONarray[x1].substring(x2+9, x3);

                if (sTyp==217) {
                    sGCKode = sGCKode + '-wp/e';
                    sRadius = 'radius=' + iRadie + ', lightyellow';
                } else if(sTyp==218) {
                    sGCKode = sGCKode + '-wv/e';
                    sRadius = 'radius=' + iRadie + ', yellow';
                } else if (sTyp==219) {
                    sGCKode = sGCKode + '-ws/e';
                    sRadius = 'radius=' + iRadie + ', yellow';
                } else if (sTyp==220) {
                    sGCKode = sGCKode + '-wf/e';
                    sRadius = 'radius=' + iRadie + ', yellow';
                } else {
                    sGCKode = '-w?';
                    sRadius = 'radius=' + iRadie + ', yellow';
                }
                if (sGCKode!=''){ // hoppar över icke fysiska wp
                    bRad = sKoord + ' <' + sGCKode + '>\r';
                    bRad = bRad + sRadius;
                    hText = hText + bRad + '\r';
                }
//217 park
//218 virtual stage
//219 stage
//220 final
//221 trailhead
//452 reference point
            }
        }

        var button;
        var bText = pTitle + '<br>';
        var a;
        if (a != a){
            bText=bText + '<br>' + hText.replaceAll('<','&lt;').replaceAll('&gt;');
        }
    //    var bText = pTitle + '<br><br>' + hText.replaceAll('<','&lt;').replaceAll('&gt;');
        button = document.createElement('div');
        button.innerHTML = bText.replace(/\r/g,'<br>');
        button.style = "top:0px;left:0px;position:absolute;border:1px ridge black;background:rgba(211,211,211,0.95);font-family:monospace;font-size: 12px;font-weight: bold;";

        // räknar ut hur lång tid skriptet tog hit ner
        var d2 = new Date();
        var n2 = d2.getTime();
        var hTitle = 'CreateGMCoords_Litmus, ';
        hTitle = hTitle + ' (' + (n2 - n1) + 'ms)';

        button.title = hTitle;
        button.addEventListener('click', function() {
            const el = document.createElement('textarea');
            el.value = hText;
            el.setAttribute('readonly', '');
            el.style.position = 'absolute';
            el.style.left = '-9999px';
            document.body.appendChild(el);
            el.select();
            document.execCommand('copy');
            document.body.removeChild(el);
        }, false);

        document.body.appendChild(button);

// Skapar variable-inställningar
//------------------------------
GM_registerMenuCommand('Ange radie', Where_Radie);
function Where_Radie() {
    var wRadie = GM_getValue('Where_Radie');
    var wRadieNEW = prompt('Texten till rese/cachegruppering)', wRadie);
    if (wRadieNEW == null) { return; }
    wRadieNEW = wRadieNEW.trim();
    GM_setValue('Where_Radie', wRadieNEW);
}

