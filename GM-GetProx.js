// ==UserScript==
// @name         GM-GetProx
// @namespace    http://tampermonkey.net/
// @version      3.00
// @description  Get prox-caches
// @updateURL    https://gitlab.com/herrFamaxe/src/raw/main/GM-GetProx.js
// @downloadURL  https://gitlab.com/herrFamaxe/src/raw/main/GM-GetProx.js
// @author       Mats
// @match        https://www.geocaching.com/*
// @icon         https://www.google.com/s2/favicons?sz=64&domain=stackoverflow.com
// @grant        GM_getValue
// @grant        GM_setValue
// ==/UserScript==

var koordinater;
var koordE2;
var gckod;
var kLatDec;
var kLonDec;
var pAdmin;
var ix1;
var ix2;

// Hämtar koordinaterna
var koordEl = document.getElementById('uxLatLon');
if (koordEl == null) { // om admin-sida
    pAdmin = 1;
    koordE2 = document.getElementById('ctl00_ContentBody_CacheDetails_Coords');
    if (koordE2 != null) {
        koordinater=koordE2.textContent;
        gckod = document.getElementById('ctl00_ContentBody_CacheDetails_WptRef').textContent;
    }
} else { //om spelar-sida
    pAdmin = 0;
    koordE2 = document.getElementById('uxLatLon');
    if (koordE2 != null) {
        koordinater=koordE2.textContent;
        gckod = document.getElementById('ctl00_ContentBody_CoordInfoLinkControl1_uxCoordInfoCode').textContent;
    }
}

if (koordinater != undefined) {
    //koordinater = 'N59 00.000, E18 00.00' //document.getElementById('uxLatLon').textContent;
    koordinater = koordinater.replace(/°/g,'');
    koordinater = koordinater.replace(' E',', E').replace(' W',', W')
    koordinater = koordinater.replace('E ','E').replace('W ','W')
    koordinater = koordinater.replace('E0','E').replace('W0','W')
    koordinater = koordinater.replace('N ','N').replace('S ','S')

    // Konverterar koordinaterna till dec-format
    ix1 = koordinater.indexOf(',')
    if (ix1 > 0) {
        kLatDec = koordinater.substring(1, ix1);
        kLonDec = koordinater.substring(ix1+3, koordinater.length);
        ix2 = kLatDec.indexOf(' ')
        if (ix2 > 0) {
            kLatDec = (parseInt(kLatDec.substring(0,ix2+1)) + parseFloat(kLatDec.substring(ix2+1,ix2+7) / 60)).toFixed(5);
        }
        ix2 = kLonDec.indexOf(' ')
        if (ix2 > 0) {
            kLonDec = (parseInt(kLonDec.substring(0,ix2+1)) + parseFloat(kLonDec.substring(ix2+1,ix2+7) / 60)).toFixed(5);
        }
        if (koordinater.substr(0,1) == 'S') {
            kLatDec = kLatDec * -1;
        }
        if (koordinater.substr(ix1+2,1) == 'W') {
            kLonDec = kLonDec * -1;
        }
    }

    const channel = new BroadcastChannel('my-channel');
    const channel2 = new BroadcastChannel('my-channel2');

    // alla sidor lyssnar på my-channel och svarar på my-channel2
    channel.addEventListener("message", e => {
        var iMessage = kLatDec + ',' + kLonDec
        if (e.data.indexOf('gckod') > 0) {
            iMessage += ' [' + gckod + ']'
        }
        channel2.postMessage(iMessage);
        console.log(iMessage);
    });

    //Nya div-rutan
    var nyDiv = document.createElement('div');
    var bText = "";
    bText += "<span id='spanKoord1' title='Get coords' onclick='klickStaket()'>&nbsp;-##-&nbsp;</span>";
    bText += "<input type='checkbox' id='chk1' name='chk1'><label for='chk1' style='display:inline-block'>Code&nbsp;</label>";
    bText += "<input type='checkbox' id='chk2' name='chk2'><label for='chk2' style='display:inline-block'>Circle&nbsp;</label>";
    bText += "<br><span id='spanKoord2'></span>";

    nyDiv.innerHTML = bText;
    nyDiv.style = "top:0px;left:0px;min-height:18px;min-width:180px;position:absolute;border:1px ridge black;background:rgba(211,211,211,0.95);font-family:monospace;font-size: 12px;font-weight: bold;text-align: left";
    nyDiv.addEventListener('click', function() {
        var utText = document.getElementById('spanKoord2').innerHTML;
        utText=utText.replaceAll('<br>','\n');
        utText=utText.replaceAll('[','<');
        utText=utText.replaceAll(']','>');
        const el = document.createElement('textarea');
        el.value = utText;
        el.setAttribute('readonly', '');
        el.style.position = 'absolute';
        el.style.left = '-9999px';
        document.body.appendChild(el);
        el.select();
        document.execCommand('copy');
        document.body.removeChild(el);
    }, false);

    //Nytt script till sidan
    document.body.appendChild(nyDiv);
    var script = document.createElement("script");

    // Add script content
    script.innerHTML  = "function klickStaket() {";
    script.innerHTML += "    ";
    script.innerHTML += "    const channel = new BroadcastChannel('my-channel');";
    script.innerHTML += "    const channel2 = new BroadcastChannel('my-channel2');";
    script.innerHTML += "    document.getElementById('spanKoord2').innerHTML = '';";
    script.innerHTML += "    var InCoord1 = [];";
    script.innerHTML += "    var chk1 = document.getElementById('chk1');";
    script.innerHTML += "    var chk2 = document.getElementById('chk2');";
    script.innerHTML += "    var utMessage = 'koordinater, ';";
    script.innerHTML += "    if (chk1.checked == true){";
    script.innerHTML += "        utMessage += 'gckod, ';";
    script.innerHTML += "    }";
    script.innerHTML += "    channel.postMessage(utMessage);";
    script.innerHTML += "    channel2.addEventListener('message', function myListener(e) {";
    script.innerHTML += "    if (!InCoord1.includes(e.data)) { ";
    script.innerHTML += "        InCoord1.push(e.data);";
    script.innerHTML += "        var utLine = e.data;";
    script.innerHTML += "        utLine += '<br>';";
    script.innerHTML += "        if (chk2.checked == true){";
    script.innerHTML += "            utLine += 'radius=80, yellow<br>';";
    script.innerHTML += "        }";
    script.innerHTML += "        document.getElementById('spanKoord2').innerHTML += utLine;";
    script.innerHTML += "    }";
    script.innerHTML += "    console.log(e.data);";
    script.innerHTML += "});";
    script.innerHTML += "}";
    document.head.appendChild(script);

}
