// ==UserScript==
// @name         ChangeLogFont
// @namespace    http://tampermonkey.net/
// @version      0.2
// @description  try to take over the world!
// @author       You
// @match        https://www.geocaching.com/live/*
// @icon         https://www.google.com/s2/favicons?sz=64&domain=geocaching.com
// @grant        none
// ==/UserScript==

(function() {
    window.onload = function ChangeFont () {
        var tEl = document.getElementById("gc-md-editor_md");
        tEl.style.fontFamily = 'arial';
        tEl.style.fontSize = '14px';
        tEl.style.lineHeight = '1.2';

        var x2 = setLogType();
        tEl.focus();

    }

})()

async function setLogType () {
    await openLogType();
    var logTypeElement;

//  var logType = document.querySelector("input[name='logType']").value;
//  alert(document.getElementsByClassName('css-sf51aj-menu')[0].innerHTML);
//  letar rätt på vilken loggtyp som kommer med in, sparar den i logTypeElement för att sätta om den längre ner
    var logArray = document.getElementsByClassName('css-sf51aj-menu')[0].innerHTML.split('css-');
    for (var i = 0; i < logArray.length; i++) {
        if (logArray[i].indexOf('wwejtu-option') != -1 || logArray[i].indexOf('14aqvgk-option') != -1 ) {
           logTypeElement=logArray[i].substring(logArray[i].indexOf('id="')+4, logArray[i].indexOf('" tabindex="'));
        }
    }

/*
    switch (logType) {
        case '18': // 18=rev note
            logTypeElement='react-select-cache-log-type-option-0';
            break;
        case '23': // 23=Enable
            logTypeElement='react-select-cache-log-type-option-1';
            break;
        case '5': // 5=archive  // 12=unarchive
            logTypeElement='react-select-cache-log-type-option-2';
            break;
       case '24': // 24=publish
            logTypeElement='react-select-cache-log-type-option-3';
            break;
       default:
            logTypeElement='react-select-cache-log-type-option-0';
            break;
    }
*/

    var tE2b = document.getElementById(logTypeElement);
    tE2b.dispatchEvent(new MouseEvent('mousedown', {'view':window,'bubbles':true,'cancelable':true}));
    tE2b.dispatchEvent(new MouseEvent('mouseup', {'view':window,'bubbles':true,'cancelable':true}));
    tE2b.dispatchEvent(new MouseEvent('click', {'view':window,'bubbles':true,'cancelable':true}));
 //   simulateMouseEvent (tE2b, "mousedown", 1, 1);
 //   simulateMouseEvent (tE2b, "mouseup", 1, 1);
 //   simulateMouseEvent (tE2b, "click", 1, 1);
}

function openLogType () {
    var tE2a = document.getElementsByClassName('css-i2jsxq-control');

    tE2a[0].dispatchEvent(new MouseEvent('mousedown', {'view':window,'bubbles':true,'cancelable':true}));
    tE2a[0].dispatchEvent(new MouseEvent('mouseup', {'view':window,'bubbles':true,'cancelable':true}));
    tE2a[0].dispatchEvent(new MouseEvent('click', {'view':window,'bubbles':true,'cancelable':true}));

}
