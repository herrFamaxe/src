// ==UserScript==
// @name         CL_updateEgna
// @namespace    http://tampermonkey.net/
// @version      2.2
// @description  Update av poster i herrFamaxeCacher
// @updateURL    https://gitlab.com/herrFamaxe/src/raw/main/CL_updateEgna.js
// @downloadURL  https://gitlab.com/herrFamaxe/src/raw/main/CL_updateEgna.js
// @author       Mats
// @match        https://www.geocaching.com/geocache/*
// @icon         data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw==
// @grant        GM_registerMenuCommand
// @grant        GM_getValue
// @grant        GM_setValue
// ==/UserScript==

(function() {
    window.onload = function SamlaInfo () {

        //Tar en tidsstämpel
        var d1 = new Date();
        var n1 = d1.getTime();
        var ix1;
        var ix2;
        var ix4;
        var ix5='';
        var ixT;
        var username='';
        var sText;
        var xpathResult;
        var node;
        var favoCount;
        var sendKod;
        var namn;
        var gckod;
        var logDate;
        var antalFound;
        var logData;
        var logString='';
        var logString2;
        var nLoop=7;
        var uDisable;

        sText = '  "username": "';
        xpathResult = document.evaluate("(//text()[contains(.,'" + sText + "')])[1]", document, null, XPathResult.FIRST_ORDERED_NODE_TYPE, null);
        node=xpathResult.singleNodeValue;
        ix1 = node.data.indexOf(sText, 1);
        ix2 = node.data.indexOf('",', ix1+1);
        if (ix1 > 0) {
            username=node.data.substring(ix1+15,ix2);
        }

        // hittar diverse information om cachen
        namn = document.getElementById('ctl00_ContentBody_CacheName').textContent;
        gckod = document.getElementById('ctl00_ContentBody_CoordInfoLinkControl1_uxCoordInfoCode').textContent;
        favoCount = document.getElementById('uxFavContainerLink').textContent;
        favoCount = favoCount.replace(/\s/g, '')
        favoCount = favoCount.replace(',','')
        favoCount = favoCount.replace('Favorites','');
     //   alert('!' + favoCount + '!');


        //Letar efter hint, sätter flaggan uHint
        //--------------------------------------
        var uHint = '';
        var nmAttrib;
        xpathResult = document.evaluate("(//text()[contains(., 'No hints available.')])[1]", document, null, XPathResult.FIRST_ORDERED_NODE_TYPE, null);
        node=xpathResult.singleNodeValue;
        if (node===null) { // det finns en hint
            nmAttrib = document.getElementsByClassName('WidgetBody')[1].innerHTML // text-elemetet för attribut, kan ligga på olika platser
            uHint='/h';
        } else {
            nmAttrib = document.getElementsByClassName('WidgetBody')[0].innerHTML // text-elemetet för attribut, kan ligga på olika platser
        }

        //Letar efter Needs maintenance-attribut, sätter flaggan uNM
        //----------------------------------------------------------
        var uNM = '';
        if (nmAttrib.indexOf('<img src="/images/attributes/firstaid-yes.png"', 0) > 0) {
            uNM = '/nm';
        } else if (nmAttrib.indexOf('<strong>No attributes available</strong>', 0) > 0) {
            uNM = '';
        } else if (nmAttrib.indexOf('<img src="/images/attributes', 0) > 0) {
            uNM = '';
        } else {
            uNM = '/-'; // om träff här är det fel text-element
        }


        //Kontroll om disabled
        uDisable = document.getElementById('ctl00_ContentBody_disabledMessage');
        if (uDisable===null) {
            uDisable='';
        } else {
            uDisable='/dis';
            uNM='';// disable trumfar över need maintemaintenancenace
        }

        //Tar datum från den senaste loggen
        xpathResult = document.evaluate("(//text()[contains(., 'initialLogs = {')])[1]", document, null, XPathResult.FIRST_ORDERED_NODE_TYPE, null);
        node=xpathResult.singleNodeValue;
        ix1= node.data.indexOf(',"Visited":"', 1)
        if (ix1 > 0) {
            logDate=node.data.substr(ix1 + 12,10)
        }

            ix2=1;
            ix4=0;
            //Loopar igenom de senaste loggarna och kollar typ
            while (ix4 < nLoop) {
                ix1 = node.data.indexOf('{"LogID":', ix2);
                ix2 = node.data.indexOf('{"LogID":', ix1 + 3);
                if (ix2 < 0) { // bryter while när sista LogID hittats
                    ix4=99;
                    ix2=node.data.length
                }
                // f d w O
                // P M U A D E R N
                logData = node.data.substring(ix1,ix2);
                if (logData.indexOf(',"LogType":"Found it"', 1) > 0) {
                    logString=logString+'f';
                } else if (logData.indexOf(',"LogType":"Didn', 1) > 0) {
                    logString=logString+'d';
                } else if (logData.indexOf(',"LogType":"Write note"', 1) > 0) {
                    logString=logString+'w';
                } else if (logData.indexOf(',"LogType":"Owner Maintenance"', 1) > 0) {
                    logString=logString+'O';
                } else if (logData.indexOf(',"LogType":"Publish Listing"', 1) > 0) {
                    logString=logString+'P';
                } else if (logData.indexOf(',"LogType":"Needs Maintenance"', 1) > 0) {
                    logString=logString+'M';
                } else if (logData.indexOf(',"LogType":"Unarchive"', 1) > 0) {
                    logString=logString+'U';
                } else if (logData.indexOf(',"LogType":"Archive"', 1) > 0) {
                    logString=logString+'A';
                } else if (logData.indexOf(',"LogType":"Temporarily Disable Listing"', 1) > 0) {
                    logString=logString+'D';
                } else if (logData.indexOf(',"LogType":"Enable Listing"', 1) > 0) {
                    logString=logString+'E';
                } else if (logData.indexOf(',"LogType":"Post Reviewer Note"', 1) > 0) {
                    logString=logString+'R';
                } else if (logData.indexOf(',"LogType":"Needs Archived"', 1) > 0) {
                    logString=logString+'N';
                } else if (logData.indexOf(',"LogType":"Webcam Photo Taken"', 1) > 0) {
                    logString=logString+'f'; // Kör samma som Found
                } else {
                    logString=logString+'*';
                }

                // kollar om loggen har mitt username, sparar då vilken logg i ordnining det är
                sText = ',"UserName":"' + username + '"'
                if (logData.indexOf(sText, 1) > 0 && ix5==='') {
                    ix5=ix4+1;
                }
                sText = ',"UserName":"swedenreviewers"'
                if (logData.indexOf(sText, 1) > 0 && ix5==='') {
                    ix5=ix4+1;
                }
                ix4++; // swedenreviewers
            }
        logString2='(' + logString + ')' + ix5 + uNM + uDisable;

        //Letar efter antal Founds, ligger alltid först
//        antalFound = document.getElementById('ctl00_ContentBody_lblFindCounts').getElementsByTagName('p')[0].textContent
        antalFound = document.getElementById('ctl00_ContentBody_lblFindCounts').innerHTML
        ix1 = antalFound.indexOf('Found it', 1)
        if (ix1 > -1) {
            antalFound = document.getElementById('ctl00_ContentBody_lblFindCounts').textContent
            ix1 = antalFound.indexOf(' ', 1)
            antalFound = antalFound.substring(1, ix1);
            antalFound = antalFound.replace(/\s/g, '')
        } else {
            antalFound=0;
        }

//=======================================================================

        // update cache i db
        var xhttp1 = new XMLHttpRequest();
        var clDiv;
        xhttp1.open("POST", "https://gctt.se/cacheList/s_saveEgna.htm", true);
        xhttp1.setRequestHeader('Content-type', 'application/x-www-form-urlencoded');

        sendKod = "gc0=" + username + "&gc1=" + gckod + "||" + encodeURIComponent(logDate) + "||" + encodeURIComponent(antalFound) + "||" + encodeURIComponent(favoCount) + "||" + encodeURIComponent(logString2) + "||";
        sendKod = sendKod.replace(/'/g,'|');
    //    alert(sendKod);

        xhttp1.send(sendKod);
        xhttp1.onreadystatechange = function() {
            if (xhttp1.readyState == XMLHttpRequest.DONE) {
               //alert(xhttp1.responseText);
                ixT = xhttp1.responseText.indexOf('OK -');
                clDiv = document.getElementById('cacheListDIV');
                if (ixT > -1) {
                    clDiv.style.backgroundColor = '#50C878'; //grönt
                 } else {
                    clDiv.style.backgroundColor = '#FF9D88';
                }
                xhttp1.close;
             }
        }

        //Bygger ihop texten
        var helText = '---Uppdate egna---\r';
        helText = helText + "<span id='cacheListDIV' style='font-size: 14px'>&nbsp;" + namn + ", " + gckod + "&nbsp;</span>\r";
        helText = helText + '&nbsp;' + logDate + ', ' + antalFound + ' (f' + favoCount + ')&nbsp;\r';
        helText = helText + '&nbsp;' + logString2 + '\r';

        // Texten helt klar
        var gcKodUT = gckod + '/d';

        // Skapar div på sidan
        var button;
        button = document.createElement('div');
        button.innerHTML = helText.replace(/\r/g,'<br>');
        button.style = "top:0px;left:0px;position:absolute;border:1px ridge black;background:rgba(211,211,211,0.95);font-family:monospace;font-size: 12px;font-weight: bold;";

        // räknar ut hur lång tid skriptet tog hit ner
        var d2 = new Date();
        var n2 = d2.getTime();
        gcKodUT = gcKodUT + ' (' + (n2 - n1) + 'ms)';
        button.title = gcKodUT;

        // skapar div på cache-sidan
        document.body.appendChild(button);

    }

})()
