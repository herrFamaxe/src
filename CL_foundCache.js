// ==UserScript==
// @name         CL_foundCache
// @namespace    http://tampermonkey.net/
// @version      2.3
// @description  Samla ihop cacheinfo på loggade cacher
// @updateURL    https://gitlab.com/herrFamaxe/src/raw/main/CL_foundCache.js
// @downloadURL  https://gitlab.com/herrFamaxe/src/raw/main/CL_foundCache.js
// @author       Mats
// @match        https://www.geocaching.com/geocache/*
// @icon         data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw==
// @grant        GM_registerMenuCommand
// @grant        GM_getValue
// @grant        GM_setValue
// ==/UserScript==

(function() {
    window.onload = function SamlaInfo () {

        //Tar en tidsstämpel
        var d1 = new Date();
        var n1 = d1.getTime();
        var ix1;
        var ix2;
        var ixT;
        var username='';
        var sText;
        var xpathResult;
        var node;
        var sendKod;
        var namn;
        var namnDB;
        var gckod;
        var koordinater1;
        var koordinater2;
        var cachetyp;
        var hidden;
        var kLatDec;
        var kLonDec;
        var logTyp;
        var logGUID;
        var logDate;
        var placedBy;
        var placedByGUID;

        if (!document.getElementById("ctl00_ContentBody_GeoNav_logText")) {
            return;
        }

        logTyp = document.getElementById("ctl00_ContentBody_GeoNav_logText").innerHTML;
        logTyp = logTyp.replace('!','');
        if (logTyp != 'Found It' && logTyp != 'Attended') {
             return;
        }

        // hittar found-log information
        sText = document.getElementById("ctl00_ContentBody_GeoNav_logDate").innerHTML;
        ix1 = sText.indexOf('LUID')
        if (ix1 > -1) {
            sText = sText.substr(ix1+5,sText.length)
            ix1=sText.indexOf('"');
            if (ix1 > -1) {
                logGUID = sText.substr(0, ix1);
                ix1=sText.indexOf('Logged on:');
                if (ix1 > -1) {
                    logDate = sText.substr(ix1+11, 10);
                }
            }
        }

        sText = '  "username": "';
        xpathResult = document.evaluate("(//text()[contains(.,'" + sText + "')])[1]", document, null, XPathResult.FIRST_ORDERED_NODE_TYPE, null);
        node=xpathResult.singleNodeValue;
        ix1 = node.data.indexOf(sText, 1);
        ix2 = node.data.indexOf('",', ix1+1);
        if (ix1 > 0) {
            username=node.data.substring(ix1+15,ix2);
        }

        placedBy = document.getElementById('ctl00_ContentBody_mcd1').innerHTML;
        ix1 = placedBy.indexOf('guid=');
        placedByGUID = placedBy.substr(ix1+5,36);
        ix1 = placedBy.indexOf('">',ix1);
        placedBy=placedBy.substr(ix1+2,placedBy.length);
        ix1 = placedBy.indexOf('</a>');
        placedBy=placedBy.substr(0,ix1);

        // hittar diverse information om cachen
        namn = document.getElementById('ctl00_ContentBody_CacheName').textContent;
        namnDB = encodeURIComponent(document.getElementById('ctl00_ContentBody_CacheName').textContent);
        gckod = document.getElementById('ctl00_ContentBody_CoordInfoLinkControl1_uxCoordInfoCode').textContent;
        hidden = document.getElementById('ctl00_ContentBody_mcd2').textContent;
        hidden = hidden.replace(/\s/g, '');
        ix1 = hidden.indexOf(':')
        if (ix1 > 0) {
            hidden = hidden.substr(ix1 + 1,10);
        }

        // hittar koordinater
        koordinater1 = document.getElementById('uxLatLon').textContent;
        koordinater1 = koordinater1.replace(/°/g,'');
        koordinater1 = koordinater1.replace(' E ',', E').replace(' W ',', W')
        koordinater1 = koordinater1.replace('E0','E').replace('W0','W')
        koordinater1 = koordinater1.replace('N ','N').replace('S ','S')

        ix1 = koordinater1.indexOf(',') // Fixar koordinaterna i dec-format
        if (ix1 > 0) {
            kLatDec = koordinater1.substring(1, ix1);
            kLonDec = koordinater1.substring(ix1+3, koordinater1.length);
            ix1 = kLatDec.indexOf(' ')
            if (ix1 > 0) {
                kLatDec = (parseInt(kLatDec.substring(0,ix1+1)) + parseFloat(kLatDec.substring(ix1+1,ix1+7) / 60)).toFixed(6);
            }
            ix1 = kLonDec.indexOf(' ')
            if (ix1 > 0) {
                kLonDec = (parseInt(kLonDec.substring(0,ix1+1)) + parseFloat(kLonDec.substring(ix1+1,ix1+7) / 60)).toFixed(6);
            }//
            koordinater2 = kLatDec + ', ' + kLonDec
        }

        // om cachen har justerade koordinater, sätter flaggan uKoord
        var uKoord ='';
        sText = '{"isUserDefined":true,"newLatLng"' // söktext för att hitta om cachen har justerade koordinater
        xpathResult = document.evaluate("(//text()[contains(.,'" + sText + "')])[1]", document, null, XPathResult.FIRST_ORDERED_NODE_TYPE, null);
        node=xpathResult.singleNodeValue;
        if (node!=null) {uKoord='!';}

        // hämtar cachetyp
        cachetyp = document.getElementsByClassName('cacheImage')[0].attributes[2].textContent;

        // extra-rad med info
        //-----------------------------
        var size;
        var dRating;
        var tRating;
        var country;
        var state;

        // land och stat
        country = document.getElementById('ctl00_ContentBody_Location').textContent;
        ix1=country.indexOf(',');
        if (ix1 > -1) {
            state = country.substr(3,ix1 - 3);
            country = country.substr(ix1+2,country.length);
        } else {
            state = '';
            country = country.substr(3,country.length);
        }

        //Letar efter storlek
        size = document.getElementsByClassName('minorCacheDetails')[2].textContent
        size = size.substr(2, 10);
        size = size.replace('(','').replace(')','');
        size = size.substr(0, 1).toUpperCase() + size.substr(1, 15);

        //När storlek inte är relevant sätts det om till cachetyp/eventtyp
        if (cachetyp==='EarthCache') {
            size='Earth'
        } else if (cachetyp==='Virtual Cache') {
            size='Virtual'
        } else if (cachetyp==='Locationless (Reverse) Cache') {
            size='Virtual'
        } else if (cachetyp==='Webcam Cache') {
            size='Webcam'
        } else if (cachetyp==='GPS Adventures Exhibit') {
            size='Maze'
        } else if (cachetyp==='Event Cache') {
            size='Event'
        } else if (cachetyp==='Mega-Event Cache') {
            size='Mega-Event'
        } else if (cachetyp==='Giga-Event Cache') {
            size='Giga-Event'
        } else if (cachetyp==='Cache In Trash Out Event') {
            size='CITO Event'
        } else if (cachetyp==='Community Celebration Event') {
            size='Celebration Event'
        } else if (cachetyp==='Geocaching HQ Celebration') {
            size='HQ Celebration Event'
        } else if (cachetyp==='Geocaching HQ Block Party') {
            size='Block Party'
        }

        //Letar efter Difficulty & Terrain
        dRating = document.getElementById('ctl00_ContentBody_uxLegendScale').getElementsByTagName('img')[0].outerHTML
        tRating = document.getElementById('ctl00_ContentBody_Localize12').getElementsByTagName('img')[0].outerHTML
        dRating = dRating.substr(24,10);
        tRating = tRating.substr(24,10);

        //Finns inte i klartext utan det är bildfiler
        if (dRating === 'stars1.gif') {
            dRating = '1.0'
        } else if (dRating === 'stars1_5.g') {
            dRating = '1.5'
        } else if (dRating === 'stars2.gif') {
            dRating = '2.0'
        } else if (dRating === 'stars2_5.g') {
            dRating = '2.5'
        } else if (dRating === 'stars3.gif') {
            dRating = '3.0'
        } else if (dRating === 'stars3_5.g') {
            dRating = '3.5'
        } else if (dRating === 'stars4.gif') {
            dRating = '4.0'
        } else if (dRating === 'stars4_5.g') {
            dRating = '4.5'
        } else if (dRating === 'stars5.gif') {
            dRating = '5.0'
        }
        if (tRating === 'stars1.gif') {
            tRating = '1.0'
        } else if (tRating === 'stars1_5.g') {
            tRating = '1.5'
        } else if (tRating === 'stars2.gif') {
            tRating = '2.0'
        } else if (tRating === 'stars2_5.g') {
            tRating = '2.5'
        } else if (tRating === 'stars3.gif') {
            tRating = '3.0'
        } else if (tRating === 'stars3_5.g') {
            tRating = '3.5'
        } else if (tRating === 'stars4.gif') {
            tRating = '4.0'
        } else if (tRating === 'stars4_5.g') {
            tRating = '4.5'
        } else if (tRating === 'stars5.gif') {
            tRating = '5.0'
        }

        //Sätter ihop extraraden
        var dtRat = dRating + '/' + tRating;

        // kollar om post redan finns i db
        var xhttp1 = new XMLHttpRequest();
        var clDiv;
        xhttp1.open("POST", "https://gctt.se/cacheList/s_getFound.htm", true);
        xhttp1.setRequestHeader('Content-type', 'application/x-www-form-urlencoded');
        sendKod = "gc0=" + username + "&gc1=" + gckod;
        sendKod = sendKod.replace(/'/g,'|');
        xhttp1.send(sendKod);
        xhttp1.onreadystatechange = function() {
            if (xhttp1.readyState == XMLHttpRequest.DONE) {
                ixT = xhttp1.responseText.indexOf('EXIST');
                clDiv = document.getElementById('cacheListDIV');
                if (ixT > -1) {
                    clDiv.style.backgroundColor = '#50C878'; //grönt
                 } else {
                    clDiv.style.backgroundColor = '#FF9D88';
                }
                xhttp1.close;
             }
        }

        var gcKodUT = gckod + '/d';
        var Resetext = GM_getValue('CacheList_Resetext');
        if (Resetext == null) { Resetext=''; }

         //Bygger ihop texten
        var helText = '---Found cache---\r';
        helText = helText + "<span id='cacheListDIV' style='font-size: 14px'>" + logTyp + ": " + logDate + "</span>\r";
        helText = helText + namn + '\r';
        helText = helText + cachetyp + '\r';
        if (Resetext.length > 0) {
            helText = helText + 'Resa: ' + Resetext + '\r';
        }

        // Skapar div på sidan
        var button;
        button = document.createElement('div');
        button.innerHTML = helText.replace(/\r/g,'<br>');
        button.style = "top:0px;left:0px;position:absolute;border:1px ridge black;background:rgba(211,211,211,0.95);font-family:monospace;font-size: 12px;font-weight: bold;";

        // räknar ut hur lång tid skriptet tog hit ner
        var d2 = new Date();
        var n2 = d2.getTime();
        gcKodUT = gcKodUT + ' (' + (n2 - n1) + 'ms)';
        button.title = gcKodUT;

        // skapar div på cache-sidan
        button.addEventListener('click', function() {
            var xhttp2 = new XMLHttpRequest();
            xhttp2.open("POST", "https://gctt.se/cacheList/s_saveFound.htm", true);
            xhttp2.setRequestHeader('Content-type', 'application/x-www-form-urlencoded');

            sendKod = "gc0=" + username + "&gc1=" + gckod + "||" + hidden + "||"+ encodeURIComponent(koordinater1 + uKoord) + "||" + encodeURIComponent(koordinater2) + "||"+ namnDB + "||" + encodeURIComponent(country) + "||" + encodeURIComponent(state) + "||" + encodeURIComponent(size) + "||" + dtRat + "||" + encodeURIComponent(placedBy) + "||" + encodeURIComponent(cachetyp) + "||" + logDate + "||" + logGUID + "||" + Resetext + "||";
            sendKod = sendKod.replace(/'/g,'');
            xhttp2.send(sendKod);
       //    alert(sendKod);
             // Ser till att svar kommer tillbaka från sidan
            xhttp2.onreadystatechange = function() {
                if (xhttp2.readyState == XMLHttpRequest.DONE) {
                 //   alert(xhttp2.responseText);
                    var NewgcKodUT='ERROR, kan inte spara.';
                    ixT = xhttp2.responseText.indexOf('NEW', 1);
                    if (ixT > 0) {
                        NewgcKodUT = gcKodUT.replace('/d','/dn');
                        clDiv = document.getElementById('cacheListDIV');
                        clDiv.style.backgroundColor = '#50C878'; //grönt
                    }
                    ixT = xhttp2.responseText.indexOf('EXIST', 1);
                    if (ixT > 0) {
                        NewgcKodUT = gcKodUT.replace('/d','/de');
                    }
                    button.title = NewgcKodUT;
                    xhttp2.close;
                }
            }

        }, false);

        document.body.appendChild(button);
    }

})()
// Skapar variable-inställningar
//------------------------------
GM_registerMenuCommand('Sätt rese/cachegruppering', CacheList_Resetext);
function CacheList_Resetext() {
    var Resetext = GM_getValue('CacheList_Resetext');
    var ResetextNEW = prompt('Texten till rese/cachegruppering)', Resetext);
    if (ResetextNEW == null) { return; }
    ResetextNEW = ResetextNEW.trim();
    GM_setValue('CacheList_Resetext', ResetextNEW);
}
