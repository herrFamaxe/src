// ==UserScript==
// @name         CreateEventStampV2
// @namespace    http://tampermonkey.net/
// @version      2.00
// @description  Samla eventinformation, datum, tid, koordinater och skapa ett klipp.
// @updateURL    https://gitlab.com/herrFamaxe/src/raw/main/CreateEventStampV2.user.js
// @downloadURL  https://gitlab.com/herrFamaxe/src/raw/main/CreateEventStampV2.user.js
// @author       Mats
// @match        https://www.geocaching.com/live/geocache/*
// @icon         data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw==
// @grant        GM_setClipboard
// @grant        GM_getValue
// @grant        GM_setValue
// @grant        GM_registerMenuCommand
// @grant        GM_addStyle
// ==/UserScript==

var signedInAs='';
(function() {
    window.onload = function GetEventInfo () {
        var cacheName='';
        var cacheType='';
        var postedCoordinates='';
        var eventStartTime='';
        var eventFakeEndTime='';
        var endDate =new Date();
        var placeDate='';
        var clipText='';
        var ix1;
        var ix2;
        var contentH = document.body.innerHTML;
        var iAdd=0;
        var loggText = '';
        // kollar cachetyp
        ix1 = contentH.indexOf('gc-geocache-icon"><title>', 0);
        ix2 = contentH.indexOf('</', ix1+2);

        cacheType=contentH.substring(ix1+25,ix2);
        if (cacheType.indexOf('Event',0) >=0 ) {
            //Sätter längd på eventet, OBS! bara en estimering
            if (cacheType.substr(0,5)=='Event') {iAdd=30;}
            if (cacheType.indexOf('Cache In Trash Out',0) >=0) {iAdd=60;}
            if (cacheType.indexOf('Community Celebration',0) >=0) {iAdd=120;}

            // cacheName
            ix1 = contentH.indexOf('cache name link"', 0);
            ix1 = contentH.indexOf('">', ix1);
            ix2 = contentH.indexOf('</', ix1+2);
            cacheName=contentH.substring(ix1+2,ix2);

            // vem som är påloggad
            ix1 = contentH.indexOf('"username":', ix1);
            if (ix1 > 0) {
                ix2 = contentH.indexOf('"', ix1+12);
                signedInAs=contentH.substring(ix1+12,ix2);
            }
            loggText = GM_getValue('CreateEventStampV2Text_' + signedInAs, 'Eventet äger rum #placeDate mellan klockan #eventStartTime och #eventFakeEndTime vid #postedCoordinates\nAlla ändringar utöver flytt under 161 meter måste göras av en reviewer.');
            //tar fram koordinater
            ix1 = contentH.indexOf('"postedCoordinates":{"', 0);
            if (ix1 > 0) {
                ix1 = contentH.indexOf('"latitude"', ix1);
                ix2 = contentH.indexOf('}', ix1+1);
                if (ix1 > 0) {
                    postedCoordinates=contentH.substring(ix1+11,ix2).replace('"longitude":','');
                    postedCoordinates=konvertCoords_toDDM_JS(postedCoordinates);
                }
            }

            //placedDate
            ix1 = contentH.indexOf('"placedDate":"20',0);
            ix2 = contentH.indexOf(':00"', ix1+1);
            if (ix1 > 0) {
                placeDate=contentH.substring(ix1+14,ix2-6);
                eventStartTime = contentH.substr(ix1+25,5);
                if (iAdd > 0) {
                    endDate=new Date(new Date(contentH.substring(ix1+14,ix2+3)));
                    endDate.setMinutes(parseInt(endDate.getMinutes()+iAdd));
                    eventFakeEndTime = ('0'+ endDate.getHours()).slice(-2) + ':' + ('0'+ endDate.getMinutes()).slice(-2);
                }
            }
//Tags: #placeDate #eventStartTime #eventFakeEndTime #postedCoordinates #signedInAs #cacheName
            loggText = loggText.replaceAll(/#signedInAs/gi,signedInAs);
            loggText = loggText.replaceAll(/#postedCoordinates/gi,postedCoordinates);
            loggText = loggText.replaceAll(/#placeDate/gi,placeDate);
            loggText = loggText.replaceAll(/#eventStartTime/gi,eventStartTime);
            loggText = loggText.replaceAll(/#eventFakeEndTime/gi,eventFakeEndTime);
            loggText = loggText.replaceAll(/#cacheName/gi,cacheName);
            navigator.clipboard.writeText(loggText);

            //Sätter icon på format-raden för att visa att skriptet är kört
            var labelText = document.createElement('span');
            labelText.appendChild(document.createTextNode(' '));
            var page = document.getElementsByClassName("button-type-redo")[0];
            page.appendChild(labelText);

            let img = document.createElement('img');
            img.src = 'data:image/jpg;base64,iVBORw0KGgoAAAANSUhEUgAAACwAAAAuCAYAAABTTPsKAAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAAJcEhZcwAADsMAAA7DAcdvqGQAAAKRSURBVGhD7ZnfS1NhHMbPzsntbKmz5dxSS8gU+o0WRFeFZGA/RFc3Rlddeuf/EFF/QRcRXXoRc828SYQg2vBCCiIIIzGImstmEm7u1/Hk+5z3O0i2NocnPfk+h5eHBw57Xz579933PUcSMlk27jtHY+06s312N+LP9CJcuvkFa5URLKQdQ9gZPsHJ1iMvZZfhQ63X4U/OPBCEq5IS6gRZn9qInMyn4Efr2uHTF4N/rFEQrljBQyB7wNmCmNc1+Nr6xZS4OlN0bYJwWYWOgKzfYexZG19CbDUGl258/uuaBOFS6py8BLIJXl8dih3+LUVk5ytaiyC8USPv7oLsw/lRZA/1CNlf8NsH++GPuu8JwtUp2MbrbTNidi0Hb7DXwecuv9zUGgRhkho+BrLuGoMk1dsF6m8Dc1XNLQhfid4B2UjiDbJLccJj6QW4FKis3paSIExnsmanD3E5Z9TbodZr8Mfd9wXhqkQnB6/Dg0z9rUM2eoavfZEtmWv3Ee6LGFUhuvQW2aWo8Er7281qF+7hscO8KvgR4+kfcG3w45aSJRU+dO/4SUyczCeRFXkPXBsoM/E/XnBhS7BfMxtsYjYUfpWSGj6us+FTmyQ2VrU0xjnPaQyzZN09vH/iLL5aOmvleN9KJ4TZ3incG5gexn1Ti1FmhaoQzxhbQR/8ZMpWIFmXcO34KZCrr6lFJq3wH2GP9zz8Rfw1nJ4yfs8k4PmBWVPJkqxLWA51gLBf9SJvVEbLwmmPU27ivcOH3klBuJgKVNzPu0BY5QQVW/EarOu4zbReoZysS5hUqlqQzP7rLSfrE5aetoBwo8t4UiPzW1Y0491Dqv/9tpAl/QeEuS68ugXS9FZnpufZtpIlWYywJP0GSFvJ25oD99AAAAAASUVORK5CYII=';

            img.height = 21;
            img.width = 18;
            img.title = cacheName;
            page.appendChild(img);

        }
    }
})()

function konvertCoords_toDDM_JS(koordinaterDD) { // Konverterar från DD till DDM
    if (typeof(koordinaterDD) == "undefined") {
        koordinaterDD='';
        return;
    }
    koordinaterDD = koordinaterDD.replace(' ','');
    var ix1 = koordinaterDD.indexOf (',');
    var ix2;
    var latitudDD = koordinaterDD.substring(0,ix1);
    var longitudeDD = koordinaterDD.substring(ix1+1);
    var latitudDMM = latitudDD.substring(0,latitudDD.indexOf('.'));
    var longitudeDMM = longitudeDD.substring(0,longitudeDD.indexOf('.'));
    if (latitudDD.substring(0,1)==='-') {
        latitudDMM = 'S' + latitudDMM.substring(1).padStart(2,'0');
    } else {
        latitudDMM = 'N' + latitudDMM.padStart(2,'0');
    }
    ix2 = latitudDD.substring(latitudDD.indexOf('.')) * 60;
    ix2 = ix2.toFixed(3);
    if (parseInt(ix2) < 10) {
        ix2='0' + ix2;
    }
    latitudDMM += ' ' + ix2;
    if (longitudeDD.substring(0,1)==='-') {
        longitudeDMM = 'W' + longitudeDMM.substring(1).padStart(2,'0');
    } else {
        longitudeDMM = 'E' + longitudeDMM.padStart(2,'0');
    }
    ix2 = longitudeDD.substring(longitudeDD.indexOf('.')) * 60;
    ix2 = ix2.toFixed(3);
    if (parseInt(ix2) < 10) {
        ix2='0' + ix2;
    }
    longitudeDMM += ' ' + ix2;
    return latitudDMM + ' ' + longitudeDMM;
}


// Visa config-bild
function showConfig() {

    createConfigDivHead();
    var divSet = document.getElementById('gm_divSet');
    var nWidth = parseInt(divSet.style.width);
    var gmText;
    nWidth=nWidth-20 + 'px';

    // Text
    var pBR0 = document.createElement('br');
    var pText = document.createElement('p');
    pText.setAttribute('class', 'SettingElement');

    var labelText = document.createElement('label');
    labelText.setAttribute('for', 'txtText');
    labelText.appendChild(document.createTextNode('Text, (paste it as log-text with CTRL-V)'));

    var txtText = document.createElement('textarea');
    txtText.id = 'txtText';
    txtText.style.marginRight = '6px';
    txtText.rows='7';
    txtText.style.width = nWidth;

    gmText = GM_getValue('CreateEventStampV2Text_' + signedInAs, 'Eventet äger rum #placeDate mellan klockan #eventStartTime och #eventFakeEndTime vid #postedCoordinates\nAlla ändringar utöver flytt under 161 meter måste göras av en reviewer.');
    txtText.value = gmText;

    pText.appendChild(labelText);
    pText.appendChild(pBR0);
    pText.appendChild(txtText);
    txtText.addEventListener('focus', configSelectAllText, true);
    divSet.appendChild(pText);
    createConfigDivFoot()
}

function createConfigDivHead() {

    var divSet = document.getElementById("gm_divSet");
    if (divSet) {
        var YOffsetVal = parseInt(divSet.getAttribute('YOffsetVal', '0'));
        window.scrollTo(window.pageXOffset, YOffsetVal);
        alert('Edit Setting interface already on screen.');
        return;
    }
    GM_addStyle('.SettingTitle {font-size: medium; font-style: italic; font-weight: bold; text-align: center; margin-bottom: 6px; !important; } ' );
    GM_addStyle('.SettingVersionTitle {font-size: small; font-style: italic; font-weight: bold; text-align: center; margin-bottom: 12px; !important; } ' );
    GM_addStyle('.SettingElement {text-align: left; margin-left: 6px; margin-right: 6px; margin-bottom: 12px; !important; } ' );
    GM_addStyle('.SettingButtons {text-align: right; margin-left: 6px; margin-right: 6px; margin-bottom: 6px; !important; } ' );
    document.body.setAttribute('style', 'height:100%;');

    var popwidth = parseInt(window.innerWidth * .6);
    divSet = document.createElement('div');
    divSet.id = 'gm_divSet';
    divSet.setAttribute('style', 'position: absolute; z-index: 1000; visibility: hidden; padding: 5px; outline: 7px ridge blue; background: #FFFFCC;');
    divSet.style.width = popwidth + 'px';
    divSet.setAttribute('winTopPos', document.documentElement.scrollTop); // snapback code
    divSet.style.visibility = 'visible';

	// Create heading i setup-bild
	var ds_Heading = document.createElement('div');
	ds_Heading.setAttribute('class', 'SettingTitle');
	ds_Heading.appendChild(document.createTextNode('CreateEventStampV2'));
	divSet.appendChild(ds_Heading);
	var ds_Version = document.createElement('div');
	ds_Version.setAttribute('class', 'SettingVersionTitle');
	ds_Version.appendChild(document.createTextNode('Version ' + GM_info.script.version + ', (' + signedInAs + ')'));
	divSet.appendChild(ds_Version);

    var toppos = parseInt(window.pageYOffset + 60);
    var leftpos = parseInt((window.innerWidth / 2) - (popwidth / 2));
    divSet.style.top = toppos + 'px';
    divSet.style.left = leftpos + 'px';
    divSet.setAttribute('YOffsetVal', window.pageYOffset);

    document.body.appendChild(divSet);
}

function createConfigDivFoot() {

    var divSet = document.getElementById("gm_divSet");
    var ds_Version = document.createElement('div');

    GM_addStyle('.SettingVersionTitle {font-size: small; font-style: italic; font-weight: bold; text-align: center; margin-bottom: 12px; !important; } ' );

    var ds_tips= document.createElement('div');
    ds_tips.setAttribute('class', 'SettingVersionTitle');
    ds_tips.appendChild(document.createTextNode('Tags: #placeDate #eventStartTime #eventFakeEndTime #postedCoordinates #signedInAs #cacheName'));
    divSet.appendChild(ds_tips);

    // Skapa Save/Cancel knappar
    var ds_ButtonsP = document.createElement('div');
    ds_ButtonsP.setAttribute('class', 'SettingButtons');

    var ds_SaveButton = document.createElement('button');
    ds_SaveButton = document.createElement('button');
    ds_SaveButton.appendChild(document.createTextNode("Save"));
    ds_SaveButton.addEventListener("click", configSaveButtonClicked, true);

    var ds_CancelButton = document.createElement('button');
    ds_CancelButton.style.marginLeft = '6px';
    ds_CancelButton.addEventListener("click", configCancelButtonClicked, true);
    ds_CancelButton.appendChild(document.createTextNode("Cancel"));

    ds_ButtonsP.appendChild(ds_SaveButton);
    ds_ButtonsP.appendChild(ds_CancelButton);

    var ds_Heading = document.createElement('div');
    ds_Heading.setAttribute('class', 'SettingTitle');
    ds_Heading.appendChild(ds_ButtonsP);
    divSet.appendChild(ds_Heading);

    document.body.appendChild(divSet);
}

function configCancelButtonClicked() {
    configCloseSettingsDiv();
}

function configSetLeftPos() {
    var divSet = document.getElementById('gm_divSet');
    if (divSet) {
        var popwidth = parseInt(window.innerWidth * .5);
        divSet.style.width = popwidth + 'px';
        var leftpos = parseInt((window.innerWidth / 2) - (popwidth / 2));
        divSet.style.left = leftpos + 'px';
    }
}

function configSaveButtonClicked() {
    var txtText = document.getElementById('txtText');
    GM_setValue('CreateEventStampV2Text_' + signedInAs, txtText.value.trim());
    configCloseSettingsDiv();
}

function configCloseSettingsDiv() {
    var divSet = document.getElementById('gm_divSet');
    var winTopPos = divSet.getAttribute('winTopPos') - 0; // snapback code
    removeNode(divSet);
    window.removeEventListener('resize', configSetLeftPos, true);
    document.documentElement.scrollTop = winTopPos; // snapback code
}

function configSelectAllText() {
    //this.select();
}

function removeNode(element) {
    element.parentNode.removeChild(element);
}

// Create menu option to change settings.
GM_registerMenuCommand('CreateEventStampV2 settings', showConfig);
