// ==UserScript==
// @name         CreateGMCoords
// @namespace    http://tampermonkey.net/
// @version      1.02
// @description  Get DD-coords and a GM-link
// @updateURL    https://gitlab.com/herrFamaxe/src/raw/main/CreateGMCoords.user.js
// @downloadURL  https://gitlab.com/herrFamaxe/src/raw/main/CreateGMCoords.user.js
// @author       Mats
// @match        https://www.geocaching.com/*
// @grant        GM_setClipboard
// ==/UserScript==

var gcnamn;
var gckod;
var coordinates;
var coordinatesE2;
var cLatDec;
var cLonDec;
var pAdmin;
var ix1;
var ix2;

//Get coordinates from page
var coordinatesE1 = document.getElementById('uxLatLon');
if (coordinatesE1 == null) { //if admin-page
    pAdmin = 1;
    gcnamn = document.getElementById('ctl00_ContentBody_CacheDetails_Name').textContent;
    gckod = document.getElementById('ctl00_ContentBody_CacheDetails_WptRef').textContent;
    coordinatesE2 = document.getElementById('ctl00_ContentBody_CacheDetails_Coords');
    if (coordinatesE2 != null) {
        coordinates=coordinatesE2.textContent;
    }
} else { //if player-page
    pAdmin = 0;
    gcnamn = document.getElementById('ctl00_ContentBody_CacheName').textContent;
    gckod = document.getElementById('ctl00_ContentBody_CoordInfoLinkControl1_uxCoordInfoCode').textContent;
    coordinates=coordinatesE1.textContent;
 }

if (coordinates != undefined) {
    //Clean up coordinates to N59 00.000, E18 00.00
    coordinates = coordinates.replace(/°/g,'');
    coordinates = coordinates.replace(' E',', E').replace(' W',', W')
    coordinates = coordinates.replace('E ','E').replace('W ','W')
    coordinates = coordinates.replace('E0','E').replace('W0','W')
    coordinates = coordinates.replace('N ','N').replace('S ','S')

    //Convert to dec-format, six decimals
    ix1 = coordinates.indexOf(',')
    if (ix1 > 0) {
        cLatDec = coordinates.substring(1, ix1);
        cLonDec = coordinates.substring(ix1+3, coordinates.length);
        ix2 = cLatDec.indexOf(' ')
        if (ix2 > 0) {
            cLatDec = (parseInt(cLatDec.substring(0,ix2+1)) + parseFloat(cLatDec.substring(ix2+1,ix2+7) / 60)).toFixed(6);
        }
        ix2 = cLonDec.indexOf(' ')
        if (ix2 > 0) {
            cLonDec = (parseInt(cLonDec.substring(0,ix2+1)) + parseFloat(cLonDec.substring(ix2+1,ix2+7) / 60)).toFixed(6);
        }
        if (coordinates.substr(0,1) == 'S') {
            cLatDec = cLatDec * -1;
        }
        if (coordinates.substr(ix1+2,1) == 'W') {
            cLonDec = cLonDec * -1;
        }
    }

    //Skapar ikon efter koordinaterna
    var page;
    let img = document.createElement('img');
    img.src = "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABAAAAAQCAYAAAAf8/9hAAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAAJcEhZcwAADsMAAA7DAcdvqGQAAAEySURBVDhPY7SysvjPgAUsWbyUQUlZmRHKxQlYQERSUjKDoqISWAAGamqroSwCAOSCvXv3/v/16xcKnjCh//+6tauxug4ZMEFpDKChrs7AxcXNsGP7tv8wDJVCAWAvYANNzU1QFgKAXKugoMCwbNkKRNjg8kJ6ehoKH6QGpPbEieP/XV2d4a7B6QVcwMjImKG5uYUhKioCbAjJBjg42DEUFRUyPHjwAOx6og2wtbVlOHDgEBw3N7eCxUl2ATrAGQvIYOWKZQzv3r2F8hgY7OwcoCwiDQiPiIKyEODw4cNgGmzAvHlzGFatWgkWwAauXrnM8P37dyiPgUFJSRnKAhoQEhIKZaKCmzdvQlkMDNo6ulAWJsCZ26KjI/8nJaVAeZjg/v17QJfPxW0AKI6hTDyAgQEA5tqQ5lUiaB0AAAAASUVORK5CYII="
    img.height = 16;
    img.width = 16;
    img.title = 'Copy the coordinates ' + cLatDec + ',' + cLonDec + ' to clippboard';

    img.addEventListener('click', function(e){
         if (e.shiftKey) {
           navigator.clipboard.writeText(gcnamn + ', ' + gckod);
         } else if (e.ctrlKey) {
           navigator.clipboard.writeText('https://coord.info/' + gckod);
         } else if (e.altKey) {
           navigator.clipboard.writeText(coordinates);
         } else {
           navigator.clipboard.writeText(cLatDec + ',' + cLonDec);
         }
    })

    img.addEventListener('mouseover', function(e){
         if (e.shiftKey) {
           this.title='Copy ' + gcnamn + ', ' + gckod + ' to clippboard';
         } else if (e.ctrlKey) {
           this.title='Copy https://coord.info/' + gckod + ' to clippboard';
         } else if (e.altKey) {
           this.title='Copy the coordinates ' + coordinates + ' to clippboard';
         } else {
           this.title='Copy the coordinates ' + cLatDec + ',' + cLonDec + ' to clippboard';
         }
     })

    //Create google-maps link and show the coordinates
    var googleUrl = "<a href='https://maps.google.com/?z=5&q=" + cLatDec + ',' + cLonDec + "' title='Link for Google Maps2 ' target='_blank'>" + cLatDec + "," + cLonDec + "</a>&nbsp;";
    if (pAdmin==0) {
        page = document.getElementById('ctl00_ContentBody_LocationSubPanel');
        document.getElementById('ctl00_ContentBody_LocationSubPanel').innerHTML = googleUrl ;
        page.appendChild(img)
    } else {
        page = document.getElementById('ctl00_ContentBody_CacheDetails_Coords');
        document.getElementById('ctl00_ContentBody_CacheDetails_Coords').innerHTML += '<br>' + googleUrl;
        page.appendChild(img);
    }
}

