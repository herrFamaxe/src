// ==UserScript==
// @name         SamlaInfo_KLICK_V4
// @namespace    http://tampermonkey.net/
// @version      4.57
// @description  Samla ihop cacheinformation.
// @updateURL    https://gitlab.com/herrFamaxe/src/raw/main/SamlaInfo_KLICK_V4.js
// @downloadURL  https://gitlab.com/herrFamaxe/src/raw/main/SamlaInfo_KLICK_V4.js
// @author       Mats
// @match        https://www.geocaching.com/geocache/*
// @icon         data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw==
// @grant        GM_setClipboard
// @grant        GM_registerMenuCommand
// @grant        GM_getValue
// @grant        GM_setValue
// ==/UserScript==

var nLoop=7;
var username='';
var sText;
var xpathResult;
var node;
var xDB='';
var xDB1='';
var xAuto='';
var xTid='';
var xLatDec='';
var xLonDec='';
var xLista='Lista';
(function() {
    window.onload = function SamlaInfo () {
        // Tar en tidsstämpel
        var d1 = new Date();
        var n1 = d1.getTime();

        var ix1;
        var ix2;
        var ix3;
        var ix4;

        GetParameterInfo();

        // Hämtar inloggad username
        //------------------------
        sText = '  "username": "';
        xpathResult = document.evaluate("(//text()[contains(.,'" + sText + "')])[1]", document, null, XPathResult.FIRST_ORDERED_NODE_TYPE, null);
        node=xpathResult.singleNodeValue;
        var fx1 = node.data.indexOf(sText, 1);
        var fx2 = node.data.indexOf('",', fx1+1);
        if (fx1 > 0) {
            username=node.data.substring(fx1+15,fx2);
        }

        //Hämtar diverse information om cachen
        //------------------------------------
        var namn;
        var namnDB;
        var radDB='';
        var gckod;
        var koordinater;
        var cachetyp;
        var hidden;
        var ctyp='';
        var xRadUT='';
        var kLatDec;
        var kLonDec;
        var coGUID;

        namn = document.getElementById('ctl00_ContentBody_CacheName').textContent;
        namnDB = encodeURIComponent(document.getElementById('ctl00_ContentBody_CacheName').textContent);
        gckod = document.getElementById('ctl00_ContentBody_CoordInfoLinkControl1_uxCoordInfoCode').textContent;

        // Tar fram COs GUID
        coGUID = document.getElementById('ctl00_ContentBody_mcd1').innerHTML;
        coGUID = coGUID.replace(/\s/g, '');
        ix1 = coGUID.indexOf('guid=')
        if (ix1 > 0) {
            coGUID = coGUID.substr(ix1 + 5,36);
        }

        // Tar fram cachens hidden date
        hidden = document.getElementById('ctl00_ContentBody_mcd2').textContent;
        hidden = hidden.replace(/\s/g, '');
        ix1 = hidden.indexOf(':')
        if (ix1 > 0) {
            hidden = hidden.substr(ix1 + 1,10);
        }

        // Tar fram cachens koordinater, är koordinarerna justerade blir det de justerade
        koordinater = document.getElementById('uxLatLon').textContent;
        koordinater = koordinater.replace(/°/g,'');
        koordinater = koordinater.replace(' E ',', E').replace(' W ',', W')
        koordinater = koordinater.replace('E0','E').replace('W0','W')
        koordinater = koordinater.replace('N ','N').replace('S ','S')

        ix1 = koordinater.indexOf(',') // Fixar koordinaterna i dec-format för att kunna räkna ut avstånd
        if (ix1 > 0) {
            kLatDec = koordinater.substring(1, ix1);
            kLonDec = koordinater.substring(ix1+3, koordinater.length);
            ix1 = kLatDec.indexOf(' ')
            if (ix1 > 0) {
                kLatDec = (parseInt(kLatDec.substring(0,ix1+1)) + parseFloat(kLatDec.substring(ix1+1,ix1+7) / 60)).toFixed(6);
            }
            ix1 = kLonDec.indexOf(' ')
            if (ix1 > 0) {
                kLonDec = (parseInt(kLonDec.substring(0,ix1+1)) + parseFloat(kLonDec.substring(ix1+1,ix1+7) / 60)).toFixed(6);
            }//
        }

        // Kontroll om cachen har justerade koordinater, sätter flaggan uKoord
        //-------------------------------------------------------------------
        var uKoord ='';
        sText = '{"isUserDefined":true,"newLatLng"' // söktext för att hitta om cachen har justerade koordinater
        xpathResult = document.evaluate("(//text()[contains(.,'" + sText + "')])[1]", document, null, XPathResult.FIRST_ORDERED_NODE_TYPE, null);
        node=xpathResult.singleNodeValue;
        if (node!=null) {uKoord='!';}

        // Sätter flaggan uHint om det finns en hint
        //--------------------------------------
        var uHint = '';
        var nmAttrib;
        xpathResult = document.evaluate("(//text()[contains(., 'No hints available.')])[1]", document, null, XPathResult.FIRST_ORDERED_NODE_TYPE, null);
        node=xpathResult.singleNodeValue;
        if (node===null) { // det finns en hint
            nmAttrib = document.getElementsByClassName('WidgetBody')[1].innerHTML // text-elemetet för attribut, kan ligga på olika platser
            uHint='/h';
        } else {
            nmAttrib = document.getElementsByClassName('WidgetBody')[0].innerHTML // text-elemetet för attribut, kan ligga på olika platser
        }

        // Letar efter Needs maintenance-attribut, sätter flaggan uNM
        //----------------------------------------------------------
        var uNM = '';
        if (nmAttrib.indexOf('<img src="/images/attributes/firstaid-yes.png"', 0) > 0) {
            uNM = '/nm';
        } else if (nmAttrib.indexOf('<strong>No attributes available</strong>', 0) > 0) {
            uNM = '';
        } else if (nmAttrib.indexOf('<img src="/images/attributes', 0) > 0) {
            uNM = '';
        } else {
            uNM = '/-'; // om träff här är det fel text-element
        }

        // Tar fram cachetyp och skapar ett eget cachetyp suffix
        //---------------------------------------------------
        cachetyp=document.getElementsByClassName('cacheImage')[0].attributes[2].textContent;
        if (cachetyp==='Traditional Geocache') {
            ctyp='TRAD';
        } else if (cachetyp==='Traditional Cache') {
            ctyp='TRAD';
        } else if (cachetyp==='Multi-cache') {
            ctyp='MULTI';
        } else if (cachetyp==='Multi-Cache') {
            ctyp='MULTI';
        } else if (cachetyp==='Mystery Cache') {
            ctyp='MYST';
        } else if (cachetyp==='EarthCache') {
            ctyp='EC';
        } else if (cachetyp==='Virtual Cache') {
            ctyp='VC';
        } else if (cachetyp==='Wherigo Cache') {
            ctyp='WGO';
        } else if (cachetyp==='Letterbox Hybrid') {
            ctyp='LB';
        } else if (cachetyp==='Webcam Cache') {
            ctyp='WC';
        } else if (cachetyp==='Locationless (Reverse) Cache') {
            ctyp='LC';
        } else if (cachetyp==='GPS Adventures Exhibit') {
            ctyp='MAZE';
        } else if (cachetyp==='Geocaching HQ') {
            ctyp='HQ';
        } else if (cachetyp==='Project APE Cache') {
            ctyp='APE';
        } else if (cachetyp==='Event Cache' || cachetyp==='Mega-Event Cache' || cachetyp==='Giga-Event Cache' || cachetyp.substr(0,18)==='Cache In Trash Out' || cachetyp==='Community Celebration Event' || cachetyp==='Geocaching HQ Celebration' || cachetyp==='Geocaching HQ Block Party') {
            ctyp='EVENT';
        }

       // alert (cachetyp + ' --- ' + ctyp);

        // Skapar extra-rad med info
        //-------------------------
        var antalLottText;
        var antalLottText2;
        var antalWillAttendPar=0;
        var antalWillAttend=0;
        var antalAttend=0;
        var antalText;
        var antalFound;
        var size;
        var dRating;
        var tRating;
        var ix11=0;
        var ix22=0;

        // Letar efter Will Attende och Founds, lite krångel då det kan ligga på olika ställen
        if (ctyp.substr(0,5) ==='EVENT') {
            //Plockar hela texten, kontrollera de fem första alt-paren och letar var Will Attende ligger, anväder sedan detta värde för att hitta antalet
            antalLottText = document.getElementById('ctl00_ContentBody_lblFindCounts').innerHTML;
            antalLottText=antalLottText.toLowerCase()
            ix2=1;
            ix4=0;
            // Loopar igenom alt-taggarna och letar efter Will Attend
            while (ix4 < 5) {
                //Letar mellan första alt-paret
                ix1 = antalLottText.indexOf('alt=', ix2)
                ix2 = antalLottText.indexOf('alt=', ix1 + 3)
                antalLottText2 = antalLottText.substring(ix1,ix2)
                ix3 = antalLottText2.indexOf('will attend', 1)
                if (ix3 > -1) {
                    antalWillAttendPar = ix4 + 1;
                    ix22=ix2; // sparar för att senare fortsätta söka
                    ix4=99; // Hittat, bryter loopen
                }
                ix4++;
            }

            // Vet nu var Will Attende ligger
            if (antalWillAttendPar > 0) {
                antalWillAttend = document.getElementById('ctl00_ContentBody_lblFindCounts').textContent;
                ix1=0;
                if (antalWillAttendPar > 1) {
                    ix1 = antalWillAttend.indexOf(' ', ix1 +1)
                }
                if (antalWillAttendPar > 2) {
                    ix1 = antalWillAttend.indexOf(' ', ix1 +1)
                }
                if (antalWillAttendPar > 3) {
                    ix1 = antalWillAttend.indexOf(' ', ix1 +1)
                }
                if (antalWillAttendPar > 4) {
                    ix1 = antalWillAttend.indexOf(' ', ix1 +1)
                }
                ix2 = antalWillAttend.indexOf(' ', ix1 +1)
                antalWillAttend = antalWillAttend.substring(ix1+1, ix2);
                antalWillAttend = antalWillAttend.replace(/\s/g, '')
            }

            // Antal Attended kommer efter, om det finns några
            ix11 = antalLottText.indexOf('alt=', ix22) // är ix2 från tidigare sökning
            ix22 = antalLottText.indexOf('alt=', ix11 + 3)
            antalLottText2 = antalLottText.substring(ix11,ix22)
            ix3 = antalLottText2.indexOf('attended', 1)

            if (ix3 > -1) {
                antalAttend = document.getElementById('ctl00_ContentBody_lblFindCounts').textContent;
                ix11 = antalAttend.indexOf(' ', ix1 + 1) // ska vara ix1 från tidigare sökning
                ix22 = antalAttend.indexOf(' ', ix11 + 1)
                antalAttend = antalAttend.substring(ix11 + 1, ix22);
                antalAttend = antalAttend.replace(/\s/g, '')
            }

        } else { // inte event
            antalFound = 0;
            antalText = document.getElementById('ctl00_ContentBody_lblFindCounts').innerHTML;
            ix1=antalText.indexOf('"Found it">');

            if (ix1 > 0) {
                ix2=antalText.indexOf('</li>', ix1);
                antalFound = antalText.substring(ix1+12, ix2);
            }
        }

        // Tar fram cachens storlek
        size = document.getElementsByClassName('minorCacheDetails')[2].textContent
        size = size.substr(2, 10);
        size = size.replace('(','').replace(')','');
        size = size.substr(0, 1).toUpperCase() + size.substr(1, 15);

        // Om storlek inte är relevant sätts det om till cachetyp/eventtyp
        if (ctyp.substr(0,2) === 'EC') {
            size='Earth'
        } else if (ctyp.substr(0,2) ==='VC') {
            size='Virtual'
        } else if (ctyp.substr(0,2) ==='LC') {
            size='Virtual'
        } else if (ctyp.substr(0,2) ==='WC') {
            size='Webcam'
        } else if (ctyp.substr(0,2) ==='MA') {
            size='Maze'
        } else if (ctyp.substr(0,5) ==='EVENT') {
            if (cachetyp==='Event Cache') {
                size='Event'
            } else if (cachetyp==='Mega-Event Cache') {
                size='Mega-Event'
            } else if (cachetyp==='Giga-Event Cache') {
                size='Giga-Event'
            } else if (cachetyp==='Cache In Trash Out Event') {
                size='CITO Event'
            } else if (cachetyp==='Community Celebration Event') {
                size='Celebration Event'
            } else if (cachetyp==='Geocaching HQ Celebration') {
                size='HQ Celebration Event'
            } else if (cachetyp==='Geocaching HQ Block Party') {
                size='Block Party'
            }
        }

        // Tar fram Difficulty & Terrain
        dRating = document.getElementById('ctl00_ContentBody_uxLegendScale').getElementsByTagName('img')[0].outerHTML
        tRating = document.getElementById('ctl00_ContentBody_Localize12').getElementsByTagName('img')[0].outerHTML
        dRating = dRating.substr(24,10);
        tRating = tRating.substr(24,10);

        //Finns inte i klartext utan det är bildfiler
        if (dRating === 'stars1.gif') {
            dRating = '1'
        } else if (dRating === 'stars1_5.g') {
            dRating = '1.5'
        } else if (dRating === 'stars2.gif') {
            dRating = '2'
        } else if (dRating === 'stars2_5.g') {
            dRating = '2.5'
        } else if (dRating === 'stars3.gif') {
            dRating = '3'
        } else if (dRating === 'stars3_5.g') {
            dRating = '3.5'
        } else if (dRating === 'stars4.gif') {
            dRating = '4'
        } else if (dRating === 'stars4_5.g') {
            dRating = '4.5'
        } else if (dRating === 'stars5.gif') {
            dRating = '5'
        }
        if (tRating === 'stars1.gif') {
            tRating = '1'
        } else if (tRating === 'stars1_5.g') {
            tRating = '1.5'
        } else if (tRating === 'stars2.gif') {
            tRating = '2'
        } else if (tRating === 'stars2_5.g') {
            tRating = '2.5'
        } else if (tRating === 'stars3.gif') {
            tRating = '3'
        } else if (tRating === 'stars3_5.g') {
            tRating = '3.5'
        } else if (tRating === 'stars4.gif') {
            tRating = '4'
        } else if (tRating === 'stars4_5.g') {
            tRating = '4.5'
        } else if (tRating === 'stars5.gif') {
            tRating = '5'
        }

        // Sätter ihop extraraden
        xRadUT = size + ', D' + dRating + '/T' + tRating ;
        if (ctyp.substr(0,5) ==='EVENT') {
            xRadUT = xRadUT + ', (' + antalWillAttend + 'w/' + antalAttend + 'a)'
        } else if (ctyp.substr(0,2) !== 'MA' && ctyp.substr(0,2) !== 'WC') {
            xRadUT = xRadUT + ', (' + antalFound + 'f)'
        }

        // Letar igenom de senaste loggarna, om inte Event eller Maze, kolla nLoop-antal loggar
        //------------------------------------------------------------------------------------
        if (ctyp.substr(0,2) !== 'EV' && ctyp.substr(0,2) !== 'MA') {
            var logData;
            var logString='';
            var logDate='';
            var ix5='';

            xpathResult = document.evaluate("(//text()[contains(., 'initialLogs = {')])[1]", document, null, XPathResult.FIRST_ORDERED_NODE_TYPE, null);
            node=xpathResult.singleNodeValue;

            // Tar datum från den senaste loggen
            ix3= node.data.indexOf(',"Visited":"', 1)
            if (ix3 > 0) {
                logDate=node.data.substr(ix3 + 12,10)
            }

            ix2=1;
            ix4=0;
            // Loopar igenom de senaste loggarna och kollar typ
            while (ix4 < nLoop) {
                ix1 = node.data.indexOf('{"LogID":', ix2);
                ix2 = node.data.indexOf('{"LogID":', ix1 + 3);
                if (ix2 < 0) { // bryter while när sista LogID hittats
                    ix4=99;
                    ix2=node.data.length
                }
                // f d w O
                // P M U A D E R N
                logData = node.data.substring(ix1,ix2);
                if (logData.indexOf(',"LogType":"Found it"', 1) > 0) {
                    logString=logString+'f';
                } else if (logData.indexOf(',"LogType":"Didn', 1) > 0) {
                    logString=logString+'d';
                } else if (logData.indexOf(',"LogType":"Write note"', 1) > 0) {
                    logString=logString+'w';
                } else if (logData.indexOf(',"LogType":"Owner Maintenance"', 1) > 0) {
                    logString=logString+'O';
                } else if (logData.indexOf(',"LogType":"Publish Listing"', 1) > 0) {
                    logString=logString+'P';
                } else if (logData.indexOf(',"LogType":"Needs Maintenance"', 1) > 0) {
                    logString=logString+'M';
                } else if (logData.indexOf(',"LogType":"Unarchive"', 1) > 0) {
                    logString=logString+'U';
                } else if (logData.indexOf(',"LogType":"Archive"', 1) > 0) {
                    logString=logString+'A';
                } else if (logData.indexOf(',"LogType":"Temporarily Disable Listing"', 1) > 0) {
                    logString=logString+'D';
                } else if (logData.indexOf(',"LogType":"Enable Listing"', 1) > 0) {
                    logString=logString+'E';
                } else if (logData.indexOf(',"LogType":"Post Reviewer Note"', 1) > 0) {
                    logString=logString+'R';
                } else if (logData.indexOf(',"LogType":"Needs Archived"', 1) > 0) {
                    logString=logString+'N';
                } else if (logData.indexOf(',"LogType":"Webcam Photo Taken"', 1) > 0) {
                    logString=logString+'f'; // Kör samma som Found
                } else {
                    logString=logString+'*';
                }

                // Kollar om loggens username = påloggad user, sparar då vilken logg i ordnining det är
                sText = ',"UserName":"' + username + '"'
                if (logData.indexOf(sText, 1) > 0 && ix5==='') {
                    ix5=ix4+1;
                }
                ix4++;
            }

            // Fyller ut logg-strängen med "-"
            while (logString.length < nLoop) {
                logString=logString+'-';
            }

            // Lägger till log-infon i xRadUT
            if (xRadUT.length === 0) {
                xRadUT = logDate + ' (' + logString + ')' + ix5;
            } else {
                xRadUT = xRadUT + ', ' + logDate + '(' + logString + ')' + ix5 ;
            }
            if (uHint!='') {
                xRadUT = xRadUT + uHint
            }
            if (uNM!='') {
                xRadUT = xRadUT + uNM
            }
        }

        // Bygger ihop texten
        var dist='';
        var helText;
        var extraWPt='';
        var extraWP='';
        var eTid;
        var kLatPark;
        var kLonPark;
        var kLatPDec;
        var kLonPDec;
        var gcKodUT = gckod;

        namnDB = namnDB;

        if (xDB1 === 'JA') {
            helText = "<span id='cacheListDIV' style='font-size: 14px'>" + namn + '(' + ctyp + ')</span>\rhttps://coord.info/' + gckod + '\r' + koordinater + uKoord + '\r';
        } else {
            helText = namn + '(' + ctyp + ')\rhttps://coord.info/' + gckod + '\r' + koordinater + uKoord + '\r';
        }
        // Lägger till xRadUT om den inte är tom
        if (xRadUT.length > 0) {
            helText = helText + xRadUT + '\r';
            radDB = radDB + encodeURIComponent(xRadUT) + '<br>';
        }

        // Lägger till Hidden-date på cacher från 2000 och 2001, tar datum & tid om Event
        if (ctyp.substr(0,5) !== 'EVENT') { //Inte Even
            if (hidden < '2002-01-01') {
                helText = helText + 'Hidden: ' + hidden + '\r';
                radDB=radDB + encodeURIComponent('Hidden: ' + hidden) + '<br>';
            }
        } else { // Om Event
            eTid = document.getElementById('mcd3').textContent.replace(/\s/g, '');
            ix1 = eTid.indexOf(':')
            if (ix1 > 0) { // det finns en tid
                eTid = '!' + eTid.substr(ix1+1,10);
                ix2 = eTid.indexOf('PM', 1)
                if (ix2 > 0) { // PM-tid
                    eTid = eTid.replace('!12:', '12:') // mitt-på-dagen
                    eTid = eTid.replace('!1:', '13:')
                    eTid = eTid.replace('!2:', '14:')
                    eTid = eTid.replace('!3:', '15:')
                    eTid = eTid.replace('!4:', '16:')
                    eTid = eTid.replace('!5:', '17:')
                    eTid = eTid.replace('!6:', '18:')
                    eTid = eTid.replace('!7:', '19:')
                    eTid = eTid.replace('!8:', '20:')
                    eTid = eTid.replace('!9:', '21:')
                    eTid = eTid.replace('!10:', '22:')
                    eTid = eTid.replace('!11:', '23:')
                } else { // AM-tid
                    eTid = eTid.replace('!12:', '00:') // midnatt
                    eTid = eTid.replace('!11:', '11:')
                    eTid = eTid.replace('!10:', '10:')
                    eTid = eTid.replace('!', '0')

                }
                eTid = eTid.substring(0, eTid.length - 2)
                helText = helText + 'Eventtid: ' + hidden + ' ' + eTid + '\r';
                radDB=radDB + encodeURIComponent('Eventtid: ' + hidden + ' ' + eTid) + '<br>';
            } else {
                helText = helText + 'Eventtid: ' + hidden + '\r';
                radDB=radDB + encodeURIComponent('Eventtid: ' + hidden) + '<br>';
            }
        }

        // Letar efter extra-WP och lägger till, kan finnas flera
        var trs = document.getElementsByTagName('tr');

        for (var i = 0; i < trs.length; i++) {
            extraWPt= '';
            extraWP ='';
            if (trs[i].innerHTML.indexOf('(Parking Area)') > -1) {
                extraWPt= 'PARK';
                extraWP = trs[i+0].getElementsByTagName('td')[5].textContent.replace(/ {2,}/g,'').replace(/\r?\n|\r/g, '').trim();
            }
            if (trs[i].innerHTML.indexOf('(Virtual Stage)') > -1) {
                extraWPt= 'STAGE-' + trs[i+0].getElementsByTagName('td')[3].textContent.replace(/ {2,}/g,'').replace(/\r?\n|\r/g, '').trim();
                if (extraWPt.trim()=='STAGE-') {
                    extraWPt= 'STAGE-' + trs[i+0].getElementsByTagName('td')[2].textContent.replace(/ {2,}/g,'').replace(/\r?\n|\r/g, '').trim();
                }
                extraWP = trs[i+0].getElementsByTagName('td')[5].textContent.replace(/ {2,}/g,'').replace(/\r?\n|\r/g, '').trim();
            }
            if (trs[i].innerHTML.indexOf('(Physical Stage)') > -1) {
                extraWPt= 'STAGE-' + trs[i+0].getElementsByTagName('td')[3].textContent.replace(/ {2,}/g,'').replace(/\r?\n|\r/g, '').trim();
                if (extraWPt.trim()=='STAGE-') {
                    extraWPt= 'STAGE-' + trs[i+0].getElementsByTagName('td')[2].textContent.replace(/ {2,}/g,'').replace(/\r?\n|\r/g, '').trim();
                }
                extraWP = trs[i+0].getElementsByTagName('td')[5].textContent.replace(/ {2,}/g,'').replace(/\r?\n|\r/g, '').trim();
            }
            if (trs[i].innerHTML.indexOf('(Reference Point)') > -1) {
                extraWPt= 'STAGE-' + trs[i+0].getElementsByTagName('td')[3].textContent.replace(/ {2,}/g,'').replace(/\r?\n|\r/g, '').trim();
                if (extraWPt.trim()=='STAGE-') {
                    extraWPt= 'STAGE-' + trs[i+0].getElementsByTagName('td')[2].textContent.replace(/ {2,}/g,'').replace(/\r?\n|\r/g, '').trim();
                }
                extraWP = trs[i+0].getElementsByTagName('td')[5].textContent.replace(/ {2,}/g,'').replace(/\r?\n|\r/g, '').trim();
            }
            if (trs[i].innerHTML.indexOf('(Trailhead)') > -1) {
                extraWPt= 'TRAIL-' + trs[i+0].getElementsByTagName('td')[3].textContent.replace(/ {2,}/g,'').replace(/\r?\n|\r/g, '').trim();
                if (extraWPt.trim()=='TRAIL-') {
                    extraWPt= 'TRAIL-' + trs[i+0].getElementsByTagName('td')[2].textContent.replace(/ {2,}/g,'').replace(/\r?\n|\r/g, '').trim();
                }
                extraWP = trs[i+0].getElementsByTagName('td')[5].textContent.replace(/ {2,}/g,'').replace(/\r?\n|\r/g, '').trim();
            }
            if (extraWP != '' && extraWP.length > 20) {
                //alert('1 ' + extraWP);
                extraWP = extraWP.replace(/°/g,'');
                extraWP = extraWP.replace(' E ',', E').replace(' W ',', W')
                extraWP = extraWP.replace('E0','E').replace('W0','W')
                extraWP = extraWP.replace('N ','N').replace('S ','S')
                ix1 = extraWP.indexOf(',');
                ix2 = 1 ; //extraWP.indexOf(' ') + 2 ;
                //alert('2 ' +extraWP);
                if (ix1 > 0) {
                    // Fixar koordinaterna i dec-format för att kunna räkna ut avstånd till huvud-koordinaterna
                    kLatPDec = extraWP.substring(ix2, ix1);
                    kLonPDec = extraWP.substring(ix1+3, extraWP.length);
                    //alert('3 ' + kLatPDec + ' -!- ' + kLonPDec);
                    ix1 = kLatPDec.indexOf(' ')
                    if (ix1 > 0) {
                        kLatPDec = (parseInt(kLatPDec.substring(0,ix1+1)) + parseFloat(kLatPDec.substring(ix1+1,ix1+7) / 60)).toFixed(6);
                    }
                    ix1 = kLonPDec.indexOf(' ')
                    if (ix1 > 0) {
                        kLonPDec = (parseInt(kLonPDec.substring(0,ix1+1)) + parseFloat(kLonPDec.substring(ix1+1,ix1+7) / 60)).toFixed(6);
                    }
                }
                extraWP = extraWPt + ' ' + extraWP;
                dist = GetDistance(kLatDec, kLonDec, kLatPDec,kLonPDec);
                helText = helText + extraWP + ', ' + dist + '\r';
                radDB=radDB + encodeURIComponent(extraWP + ', ' + dist) + '<br>';
            }
        }
        // ?? TA BORT ??
        if (xDB1 === 'JA' && xLista==="xStockholm") {
            helText = helText + 'CIRCLE ' + koordinater + ' (161)\r';
            radDB=radDB + encodeURIComponent('CIRCLE ' + koordinater + ' (161)') + '<br>';
        }

        // Texten helt klar
        if (xDB1 === 'JA' && xLista.length > 0) {
            gcKodUT = gcKodUT + '/d';
            helText = helText + '--' + xLista + '--\r';
        } else {
            gcKodUT = gcKodUT + '/kop';
            helText = helText + '-----\r';
        }

        var sendKod = "gc0=" + username + "&gc1=" + xLista + "||" + gckod + "||" + encodeURIComponent(koordinater + uKoord) + "||" + namnDB + "||" + ctyp + "||" + coGUID + "||AUTO||0||" + radDB;
        sendKod = sendKod.replace(/'/g,'|');

        // Skapar div
        var button;
        var NewgcKodUT;
        button = document.createElement('div');
        button.innerHTML = helText.replace(/\r/g,'<br>');
        button.style = "top:0px;left:0px;position:absolute;border:1px ridge black;background:rgba(211,211,211,0.95);font-family:monospace;font-size: 12px;font-weight: bold;";

        // Om auto-spar
        if (xAuto==='JA' && xDB1 === 'JA') {
            var xhttpU = new XMLHttpRequest();
            xhttpU.open("POST", "https://gctt.se/cacheResor/s_saveCache.htm", true);
            xhttpU.setRequestHeader('Content-type', 'application/x-www-form-urlencoded');
           // alert('S1=' + sendKod);
            xhttpU.send(sendKod);
            xhttpU.onreadystatechange = function() {
                if (xhttpU.readyState == XMLHttpRequest.DONE) {
                    //alert('i responseText1=' + xhttpU.responseText + ' /readyState=' + xhttpU.readyState + ' /status=' + xhttpU.status + ' /statusText=' + xhttpU.statusText + ' /responseXML=' + xhttpU.responseXML + ' /responseType=' + xhttpU.responseType);
                    var ixT;
                    var clDiv = document.getElementById('cacheListDIV');
                    NewgcKodUT='ERROR, kan inte spara.';
                    clDiv.style.backgroundColor = '#FF9D88';
                    ixT = xhttpU.responseText.indexOf('AUTO NE', 0);
                    if (ixT > -1) {
                        NewgcKodUT = gcKodUT.replace('/d','/dx-' + xhttpU.responseText.substring(xhttpU.responseText.indexOf('//', 0)+2));
                        clDiv.style.backgroundColor = '#ffc936'; //orange
                    }
                    ixT = xhttpU.responseText.indexOf('EXIST', 0);
                    if (ixT > -1) {
                        NewgcKodUT = gcKodUT.replace('/d','/du-' + xhttpU.responseText.substring(xhttpU.responseText.indexOf('//', 0)+2));
                        clDiv.style.backgroundColor = '#50C878'; //grönt
                    }
                    button.title = NewgcKodUT;
                    xhttpU.close;
                }
            }
        }

        // Tar ny tidsstämpel och räknar ut diffen i millisekunder
        var d2 = new Date();
        var n2 = d2.getTime();
        gcKodUT = gcKodUT + ' (' + (n2 - n1) + 'ms)';

        button.addEventListener('click', function() {
            if (xDB1 === 'JA') {
                var xhttp = new XMLHttpRequest();
                xhttp.open("POST", "https://gctt.se/cacheResor/s_saveCache.htm", true);
                xhttp.setRequestHeader('Content-type', 'application/x-www-form-urlencoded');
                sendKod=sendKod.replace('||AUTO||','||XX||');

               // alert('S2=' + sendKod);
                xhttp.send(sendKod);

                //Ser till att svar kommer tillbaka från ASP-sidan
                xhttp.onreadystatechange = function() {
                    if (xhttp.readyState == XMLHttpRequest.DONE) {
                        //alert('i responseText2=' + xhttp.responseText + ' /readyState=' + xhttp.readyState + ' /status=' + xhttp.status + ' /statusText=' + xhttp.statusText + ' /responseXML=' + xhttp.responseXML + ' /responseType=' + xhttp.responseType);
                        var ixT;
                        var clDiv = document.getElementById('cacheListDIV');
                        NewgcKodUT='ERROR, kan inte spara.';
                        clDiv.style.backgroundColor = '#FF9D88';
                        ixT = xhttp.responseText.indexOf('NEW', 0);
                        if (ixT > -1) {
                            NewgcKodUT = gcKodUT.replace('/d','/dn-' + xhttp.responseText.substring(xhttp.responseText.indexOf('//', 0)+2));
                            clDiv.style.backgroundColor = '#50C878'; //grönt
                        }
                        ixT = xhttp.responseText.indexOf('EXIST', 0);
                        if (ixT > -1) {
                            NewgcKodUT = gcKodUT.replace('/d','/de-' + xhttp.responseText.substring(xhttp.responseText.indexOf('//', 0)+2));
                            clDiv.style.backgroundColor = '#50C878'; //grönt
                        }
                        button.title = NewgcKodUT;
                        xhttp.close;
                    }
                }
            } else {
                const el = document.createElement('textarea');
                el.value = helText;
                el.setAttribute('readonly', '');
                el.style.position = 'absolute';
                el.style.left = '-9999px';
                document.body.appendChild(el);
                el.select();
                document.execCommand('copy');
                document.body.removeChild(el);
            }
        }, false);
        button.title = gcKodUT;
        document.body.appendChild(button);
    }

})()
//----------------------------------------------------------
//----------------------------------------------------------
//----------------------------------------------------------
//----------------------------------------------------------
//Returnerar avstånd i km/m mellan två decimal-koordinat-par
//----------------------------------------------------------
function GetDistance(lat1, lon1, lat2, lon2)
{
    if (lat1===lat2 && lon1===lon2) {
        return '0m';
    }
    var R = 6371; // km
    var dLat = getRad(lat2-lat1);
    var dLon = getRad(lon2-lon1);
    lat1 = getRad(lat1);
    lat2 = getRad(lat2);
    var a = Math.sin(dLat/2) * Math.sin(dLat/2) + Math.sin(dLon/2) * Math.sin(dLon/2) * Math.cos(lat1) * Math.cos(lat2);
    var c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1-a));
    var d = R * c;

    // returnerar meter om under 1 km, annars km med 1 eller 0 decimaler
    if (d < 1) {
        d = (d * 1000).toFixed(0) + 'm';
    } else if (d < 10) {
        d= d.toFixed(1) + 'km';
    } else {
        d= d.toFixed(0) + 'km';
    }
    //    return '(' + d + ')';
    return d;
}
function getRad(Value)
{ // Converts degrees to radians
    return Value * Math.PI / 180;
}

//Läser de olika parametervärden som används
//------------------------------------------
function GetParameterInfo() {

    var ix1;
    var ix2;

    var xDB = GM_getValue('SamlaInfo_xSparaDB'); //
    if (typeof xDB == 'undefined') {xDB=''};
    var xDBtest = xDB.toUpperCase()
    if (xDBtest.indexOf('JA,') > -1) {xDB1='JA'};//

    ix1 = xDB.indexOf(',')
    if (ix1 > -1) {
        xLista = xDB.substring(ix1+1).trim();
        // xLista=xLista.trim();
        //  ix2=xDB.indexOf(')')
        //  if (ix2>ix1) {
        //      xLista = xDB.substring(ix1+1,ix2)
        //  }
    }

    xAuto = GM_getValue('SamlaInfo_xAutoUpdateDB');
    if (typeof xAuto == 'undefined') {xAuto=''};
    xAuto= xAuto.toUpperCase()

}

// Skapar variable-inställningar
//------------------------------
GM_registerMenuCommand('Spara i databas', SamlaInfo_xSparaDB);
function SamlaInfo_xSparaDB() {
    var xSparaDB = GM_getValue('SamlaInfo_xSparaDB');
    var xSparaDBNew = prompt('Spara cachelist-information i databas? Ange JA/NEJ\nOm Ja ange då även listnamn inom parentes, tex. (Lista1)', xSparaDB);
    if (xSparaDBNew == null) { return; }
    xSparaDBNew = xSparaDBNew.replace(/\s/g,'');
    GM_setValue('SamlaInfo_xSparaDB', xSparaDBNew);
}
GM_registerMenuCommand('Auto-update', SamlaInfo_xAutoUpdateDB);
function SamlaInfo_xAutoUpdateDB() {
    var xAutoUpdateDB = GM_getValue('SamlaInfo_xAutoUpdateDB');
    var xAutoUpdateDBNew = prompt('Automatiskt uppdatera cachen om den redan finns i listan', xAutoUpdateDB);
    if (xAutoUpdateDBNew == null) { return; }
    xAutoUpdateDBNew = xAutoUpdateDBNew.replace(/\s/g,'');
    GM_setValue('SamlaInfo_xAutoUpdateDB', xAutoUpdateDBNew);
}


