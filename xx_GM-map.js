// ==UserScript==
// @name         GM-map
// @namespace    http://tampermonkey.net/
// @version      2.16
// @description  Get a Map
// @updateURL    https://gitlab.com/herrFamaxe/src/raw/main/GM-map.js
// @downloadURL  https://gitlab.com/herrFamaxe/src/raw/main/GM-map.js
// @author       Mats
// @match        https://www.geocaching.com/*
// @icon         https://www.google.com/s2/favicons?sz=64&domain=stackoverflow.com
// @grant        GM_getValue
// @grant        GM_setValue
// @grant          GM_registerMenuCommand
// @grant          GM_addStyle
// ==/UserScript==

var koordinater;
var koordE2;
var gckod;
var kLatDec;
var kLonDec;
var pAdmin;
var ix1;
var ix2;
var ix3=0;
var SignedInAs='';

// Hämtar inloggad username // OBS, nu bara på skapa-log-sidan
//------------------------
var sText = '"username":"'; // Först leta på skapa-log-sidan
var xpathResult = document.evaluate("(//text()[contains(.,'" + sText + "')])[1]", document, null, XPathResult.FIRST_ORDERED_NODE_TYPE, null);
var node=xpathResult.singleNodeValue;
if (node == null) { // om null leta på spelar sidan
    sText = '  "username": "';
    xpathResult = document.evaluate("(//text()[contains(.,'" + sText + "')])[1]", document, null, XPathResult.FIRST_ORDERED_NODE_TYPE, null);
    node=xpathResult.singleNodeValue;
    ix3=3;
}
if (node != null) {
    ix1 = node.data.indexOf(sText, 1);
    ix2 = node.data.indexOf('",', ix1+1);
    if (ix1 > 0) {
        SignedInAs=node.data.substring(ix1+12+ix3,ix2);
    }
} else { // om fortfarande null leta på rew sidan
        SignedInAs = document.getElementById('ctl00_LogoutUrl').parentElement.parentElement.querySelectorAll('a')[0].innerText;
        SignedInAs = SignedInAs.trim();
}

// Hämtar koordinaterna
var koordEl = document.getElementById('uxLatLon');
if (koordEl == null) { // om admin-sida
    pAdmin = 1;
    koordE2 = document.getElementById('ctl00_ContentBody_CacheDetails_Coords');
    if (koordE2 != null) {
        koordinater=koordE2.textContent;
        gckod = document.getElementById('ctl00_ContentBody_CacheDetails_WptRef').textContent;
    }
} else { //om spelar-sida
    pAdmin = 0;
    koordE2 = document.getElementById('uxLatLon');
    if (koordE2 != null) {
        koordinater=koordE2.textContent;
        gckod = document.getElementById('ctl00_ContentBody_CoordInfoLinkControl1_uxCoordInfoCode').textContent;
    }
 }

if (koordinater != undefined) {
    //koordinater = 'N59 00.000, E18 00.00' //document.getElementById('uxLatLon').textContent;
    koordinater = koordinater.replace(/°/g,'');
    koordinater = koordinater.replace(' E',', E').replace(' W',', W')
    koordinater = koordinater.replace('E ','E').replace('W ','W')
    koordinater = koordinater.replace('E0','E').replace('W0','W')
    koordinater = koordinater.replace('N ','N').replace('S ','S')

    // Konverterar koordinaterna till dec-format
    ix1 = koordinater.indexOf(',')
    if (ix1 > 0) {
        kLatDec = koordinater.substring(1, ix1);
        kLonDec = koordinater.substring(ix1+3, koordinater.length);
        ix2 = kLatDec.indexOf(' ')
        if (ix2 > 0) {
            kLatDec = (parseInt(kLatDec.substring(0,ix2+1)) + parseFloat(kLatDec.substring(ix2+1,ix2+7) / 60)).toFixed(5);
        }
        ix2 = kLonDec.indexOf(' ')
        if (ix2 > 0) {
            kLonDec = (parseInt(kLonDec.substring(0,ix2+1)) + parseFloat(kLonDec.substring(ix2+1,ix2+7) / 60)).toFixed(5);
        }
        if (koordinater.substr(0,1) == 'S') {
            kLatDec = kLatDec * -1;
        }
        if (koordinater.substr(ix1+2,1) == 'W') {
            kLonDec = kLonDec * -1;
        }
    }

    // Skapar google-maps länk
    var googleUrl = "<a href='https://maps.google.com/?z=5&q=" + kLatDec + ',' + kLonDec + "' title='Länk till Google Maps' target='_blank'>" + kLatDec + "," + kLonDec + "</a>";
    var referenceNode
    if (pAdmin==0) {
        document.getElementById('ctl00_ContentBody_LocationSubPanel').innerHTML = googleUrl;
        referenceNode = document.getElementById('ctl00_ContentBody_LocationSubPanel');
    } else {
        document.getElementById('ctl00_ContentBody_CacheDetails_Coords').innerHTML += '<br>' + googleUrl;
        referenceNode = document.getElementById('ctl00_ContentBody_LocationSubPanel');
    }

    const channel = new BroadcastChannel('my-channel');
    const channel2 = new BroadcastChannel('my-channel2');

    // alla sidor lyssnar på my-channel och svarar på my-channel2
    channel.addEventListener("message", e => {
        var iMessage = kLatDec + ',' + kLonDec
        if (e.data.indexOf('gckod') > 0) {
            iMessage += ' [' + gckod + ']'
        }
        channel2.postMessage(iMessage);
        console.log(iMessage);
    });

    //Nya div-rutan
    var nyDiv = document.createElement('div');
    var bText = "";
    bText += "<span id='spanKoord1' title='Get coords' onclick='klickStaket()'>&nbsp;-##-&nbsp;</span>";
    bText += "<input type='checkbox' id='chk1' name='chk1'><label for='chk1' style='display:inline-block'>Code&nbsp;</label>";
    bText += "<input type='checkbox' id='chk2' name='chk2'><label for='chk2' style='display:inline-block'>Circle&nbsp;</label>";
    bText += "<br><span id='spanKoord2'></span>";

    nyDiv.innerHTML = bText;
    nyDiv.style = "top:0px;left:0px;min-height:18px;min-width:180px;position:absolute;border:1px ridge black;background:rgba(211,211,211,0.95);font-family:monospace;font-size: 12px;font-weight: bold;text-align: left";
    nyDiv.addEventListener('click', function() {
        var utText = document.getElementById('spanKoord2').innerHTML;
        utText=utText.replaceAll('<br>','\n');
        utText=utText.replaceAll('[','<');
        utText=utText.replaceAll(']','>');
        const el = document.createElement('textarea');
        el.value = utText;
        el.setAttribute('readonly', '');
        el.style.position = 'absolute';
        el.style.left = '-9999px';
        document.body.appendChild(el);
        el.select();
        document.execCommand('copy');
        document.body.removeChild(el);
    }, false);

    //Nytt script till sidan
    document.body.appendChild(nyDiv);
    var script = document.createElement("script");

    // Add script content
    script.innerHTML  = "function klickStaket() {";
    script.innerHTML += "    ";
    script.innerHTML += "    const channel = new BroadcastChannel('my-channel');";
    script.innerHTML += "    const channel2 = new BroadcastChannel('my-channel2');";
    script.innerHTML += "    document.getElementById('spanKoord2').innerHTML = '';";
    script.innerHTML += "    var InCoord1 = [];";
    script.innerHTML += "    var chk1 = document.getElementById('chk1');";
    script.innerHTML += "    var chk2 = document.getElementById('chk2');";
    script.innerHTML += "    var utMessage = 'koordinater, ';";
    script.innerHTML += "    if (chk1.checked == true){";
    script.innerHTML += "        utMessage += 'gckod, ';";
    script.innerHTML += "    }";
    script.innerHTML += "    channel.postMessage(utMessage);";
    script.innerHTML += "    channel2.addEventListener('message', function myListener(e) {";
    script.innerHTML += "    if (!InCoord1.includes(e.data)) { ";
    script.innerHTML += "        InCoord1.push(e.data);";
    script.innerHTML += "        var utLine = e.data;";
    script.innerHTML += "        utLine += '<br>';";
    script.innerHTML += "        if (chk2.checked == true){";
    script.innerHTML += "            utLine += 'radius=80, yellow<br>';";
    script.innerHTML += "        }";
    script.innerHTML += "        document.getElementById('spanKoord2').innerHTML += utLine;";
    script.innerHTML += "    }";
    script.innerHTML += "    console.log(e.data);";
    script.innerHTML += "});";
    script.innerHTML += "}";
    document.head.appendChild(script);
}

var vText = document.getElementById('gc-md-editor_md');
if (vText != null ) {
    var NewFoot1 ='';
    NewFoot1 = GM_getValue('AutoGMText1_' + SignedInAs, 'AutoGMText1.');
    NewFoot1 = NewFoot1.replaceAll('\n', '|');
    var NewFoot2 ='';
    NewFoot2 = GM_getValue('AutoGMText2_' + SignedInAs, 'AutoGMText2.');
    NewFoot2 = NewFoot2.replaceAll('\n', '|');
    var vA = "[!Apa]";
    var vB = "[!Banan]";
    var vC = "[!Coke]";

    vText.oninput = function(){gcTextChange();};

    var script1 = document.createElement("script");
    script1.innerHTML = "function gcTextChange() {";
    script1.innerHTML += "    var NewFoot1='" + NewFoot1 + "';";
    script1.innerHTML += "    var NewFoot2='" + NewFoot2 + "';";
    //script1.innerHTML += "    var NewFoot3= ['[!a','[!b','[!c'];";
    script1.innerHTML += "    var NewFoot3= [];";
    script1.innerHTML += "        NewFoot3.push('" + vA + "');";
    script1.innerHTML += "        NewFoot3.push('" + vB + "');";
    script1.innerHTML += "        NewFoot3.push('" + vC + "');";
    //script1.innerHTML += "    alert(NewFoot3);";

    script1.innerHTML += "    if (document.getElementById('gc-md-editor_md').value.indexOf('[!') > -1) { ";
    script1.innerHTML += "         var ix1=document.getElementById('gc-md-editor_md').value.indexOf('[!') ;";
    script1.innerHTML += "         var ix2=document.getElementById('gc-md-editor_md').value.indexOf(']',ix1) ;";
    script1.innerHTML += "         if (ix1>0 && ix2 >0) {";
    script1.innerHTML += "            var ix3=NewFoot3.indexOf(document.getElementById('gc-md-editor_md').value.substring(ix1,ix2+1)) ;";
    script1.innerHTML += "            alert(document.getElementById('gc-md-editor_md').value.substring(ix1,ix2+1) + ' ix3=' + ix3);"
    script1.innerHTML += "         }"

//    script1.innerHTML += "         document.getElementById('gc-md-editor_md').value=document.getElementById('gc-md-editor_md').value.replace('[!foot1]',NewFoot1.replaceAll('|','\\n'));";
//    script1.innerHTML += "         document.getElementById('gc-md-editor_md').value=document.getElementById('gc-md-editor_md').value.replace('[!foot2]',NewFoot2.replaceAll('|','\\n'));";

    script1.innerHTML += "    }";
    script1.innerHTML += "}";
    document.head.appendChild(script1);
}

// VISA SETTINGS-BILD/INPUT
function fShowSettings() {

	fCreateSettingsDiv('GM-map settings');
	var divSet = document.getElementById('gm_divSet');

 // Text-1
	var pBR1 = document.createElement('br');
    var pText1 = document.createElement('p');
	pText1.setAttribute('class', 'SettingElement');

    var labelText1 = document.createElement('label');
	labelText1.setAttribute('for', 'txtText1');
	labelText1.appendChild(document.createTextNode('Text 1, kod [!foot1]'));

	var txtText1 = document.createElement('textarea');
	txtText1.id = 'txtText1';
	txtText1.style.marginRight = '6px';
	txtText1.rows='7';
	txtText1.cols='110';

	var AutoText1 = GM_getValue('AutoGMText1_' + SignedInAs, 'AutoGMText1.');
	txtText1.value = AutoText1;

	pText1.appendChild(labelText1);
	pText1.appendChild(pBR1);
    pText1.appendChild(txtText1);
    txtText1.addEventListener('focus', fSelectAllText, true);
	divSet.appendChild(pText1);

 // Text-2
	var pBR2 = document.createElement('br');
    var pText2 = document.createElement('p');
	pText2.setAttribute('class', 'SettingElement');

    var labelText2 = document.createElement('label');
	labelText2.setAttribute('for', 'txtText1');
	labelText2.appendChild(document.createTextNode('Text 2, kod [!foot2]'));

    var txtText2 = document.createElement('textarea');
	txtText2.id = 'txtText2';
	txtText2.style.marginRight = '6px';
	txtText2.rows='7';
	txtText2.cols='110';

	var AutoText2 = GM_getValue('AutoGMText2_' + SignedInAs, 'AutoGMText2.');
	txtText2.value = AutoText2;

    pText2.appendChild(labelText2);
	pText2.appendChild(pBR2);
    pText2.appendChild(txtText2);
    txtText2.addEventListener('focus', fSelectAllText, true);
	divSet.appendChild(pText2);
//

	// Create Save/Cancel Buttons.
	var ds_ButtonsP = document.createElement('div');
	ds_ButtonsP.setAttribute('class', 'SettingButtons');

	var ds_SaveButton = document.createElement('button');
	ds_SaveButton = document.createElement('button');
	ds_SaveButton.appendChild(document.createTextNode("Save"));
	ds_SaveButton.addEventListener("click", fSaveButtonClicked, true);

    var ds_CancelButton = document.createElement('button');
	ds_CancelButton.style.marginLeft = '6px';
	ds_CancelButton.addEventListener("click", fCancelButtonClicked, true);
	ds_CancelButton.appendChild(document.createTextNode("Cancel"));

    ds_ButtonsP.appendChild(ds_SaveButton);
	ds_ButtonsP.appendChild(ds_CancelButton);
    divSet.appendChild(ds_ButtonsP);

}

function fCreateSettingsDiv(sTitle) {

	// If div already exists, reposition browser, and show alert.
	var divSet = document.getElementById("gm_divSet");
	if (divSet) {
		var YOffsetVal = parseInt(divSet.getAttribute('YOffsetVal', '0'));
		window.scrollTo(window.pageXOffset, YOffsetVal);
		alert('Edit Setting interface already on screen.');
		return;
	}

	// Set styles for titles and elements.
	GM_addStyle('.SettingTitle {font-size: medium; font-style: italic; font-weight: bold; ' +
				'text-align: center; margin-bottom: 6px; !important; } ' );
	GM_addStyle('.SettingVersionTitle {font-size: small; font-style: italic; font-weight: bold; ' +
				'text-align: center; margin-bottom: 12px; !important; } ' );
	GM_addStyle('.SettingElement {text-align: left; margin-left: 6px; ' +
				'margin-right: 6px; margin-bottom: 12px; !important; } ' );
	GM_addStyle('.SettingButtons {text-align: right; margin-left: 6px; ' +
				'margin-right: 6px; margin-bottom: 6px; !important; } ' );

	document.body.setAttribute('style', 'height:100%;');

    // Create div, huvud setup-bild
	var popwidth = parseInt(window.innerWidth * .6);
	divSet = document.createElement('div');
	divSet.id = 'gm_divSet';
	divSet.setAttribute('style', 'position: absolute; z-index: 1000; visibility: hidden; padding: 5px; outline: 7px ridge blue; background: #FFFFCC;');
	divSet.style.width = popwidth + 'px';
	divSet.setAttribute('winTopPos', document.documentElement.scrollTop); // snapback code
    divSet.style.visibility = 'visible';

	// Create heading i setup-bild
	var ds_Heading = document.createElement('div');
	ds_Heading.setAttribute('class', 'SettingTitle');
	ds_Heading.appendChild(document.createTextNode(sTitle));
	divSet.appendChild(ds_Heading);
    // Create heading i setup-bild
	var ds_Version = document.createElement('div');
	ds_Version.setAttribute('class', 'SettingVersionTitle');
	ds_Version.appendChild(document.createTextNode('Version ' + GM_info.script.version + ', (' + SignedInAs + ')'));
	divSet.appendChild(ds_Version);

	// Add div to page.
	var toppos = parseInt(window.pageYOffset + 60);
	var leftpos = parseInt((window.innerWidth / 2) - (popwidth / 2));
	divSet.style.top = toppos + 'px';
	divSet.style.left = leftpos + 'px';
	divSet.setAttribute('YOffsetVal', window.pageYOffset);

	document.body.appendChild(divSet);
	//window.addEventListener('resize', fSetLeftPos, true);

}

function fSelectAllText() {
	this.select();
}

function fSaveButtonClicked() {

    // Save Auto Text.
	var txtAutoText1 = document.getElementById('txtText1');
	GM_setValue('AutoGMText1_' + SignedInAs, txtAutoText1.value.trim());

    var txtAutoText2 = document.getElementById('txtText2');
	GM_setValue('AutoGMText2_' + SignedInAs, txtAutoText2.value.trim());

    fCloseSettingsDiv();
}

function fCancelButtonClicked() {
    fCloseSettingsDiv();
}

// Resize/reposition on window resizing.
function fSetLeftPos() {
	var divSet = document.getElementById('gm_divSet');
	if (divSet) {
		var popwidth = parseInt(window.innerWidth * .5);
		divSet.style.width = popwidth + 'px';
		var leftpos = parseInt((window.innerWidth / 2) - (popwidth / 2));
		divSet.style.left = leftpos + 'px';
	}
}

function fCloseSettingsDiv() {

//	var divBlackout = document.getElementById('divBlackout');
	var divSet = document.getElementById('gm_divSet');
	var winTopPos = divSet.getAttribute('winTopPos') - 0; // snapback code
    removeNode(divSet);
//	removeNode(divBlackout);
	window.removeEventListener('resize', fSetLeftPos, true);
	document.documentElement.scrollTop = winTopPos; // snapback code
}

// Remove element and everything below it.
function removeNode(element) {
	element.parentNode.removeChild(element);
}

// Create Greasemonkey Tools menu option to allow user to change settings.
GM_registerMenuCommand('GM-map settings', fShowSettings);

