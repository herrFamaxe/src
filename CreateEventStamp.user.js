// ==UserScript==
// @name         CreateEventStamp
// @namespace    http://tampermonkey.net/
// @version      1.05
// @description  Samla eventinformation, datum, tid, koordinater och skapa ett klipp.
// @updateURL    https://gitlab.com/herrFamaxe/src/raw/main/CreateEventStamp.user.js
// @downloadURL  https://gitlab.com/herrFamaxe/src/raw/main/CreateEventStamp.user.js
// @author       Mats
// @match        https://www.geocaching.com/live/geocache/*
// @icon         data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw==
// @grant        GM_setClipboard
// ==/UserScript==

(function() {
    window.onload = function GetEventInfo () {
        var cacheName='';
        var cacheType=''
        var postedCoordinates='';
        var startDate;
        var endDate =new Date();
        var placeDate='';
        var clipText='';
        var ix1;
        var ix2;
        var contentH = document.body.innerHTML;
        var iAdd=0;

        // kollar cachetyp
        ix1 = contentH.indexOf('gc-geocache-icon"><title>', 0);
        ix2 = contentH.indexOf('</', ix1+2);

        cacheType=contentH.substring(ix1+25,ix2);
        if (cacheType.indexOf('Event',0) >=0 ) {
            //Sätter längd på eventet, OBS! bara en estimering
            if (cacheType.substr(0,5)=='Event') {iAdd=30;}
            if (cacheType.indexOf('Cache In Trash Out',0) >=0) {iAdd=60;}
            if (cacheType.indexOf('Community Celebration',0) >=0) {iAdd=120;}

            // cacheName
            ix1 = contentH.indexOf('cache name link"', 0);
            ix1 = contentH.indexOf('">', ix1);
            ix2 = contentH.indexOf('</', ix1+2);
            cacheName=contentH.substring(ix1+2,ix2);

            //tar fram koordinater
            ix1 = contentH.indexOf('"postedCoordinates":{"', 0);
            if (ix1 > 0) {
                ix1 = contentH.indexOf('"latitude"', ix1);
                ix2 = contentH.indexOf('}', ix1+1);
                if (ix1 > 0) {
                    postedCoordinates=contentH.substring(ix1+11,ix2).replace('"longitude":','');
                    postedCoordinates=konvertCoords_toDDM_JS(postedCoordinates);
                }
            }
            //Sätter datum, starttid och beräknad slutför eventet
            ix1 = contentH.indexOf('"placedDate":"20', 0);
            ix2 = contentH.indexOf(':00",', ix1+1);
            if (ix1 > 0) {
                if (iAdd > 0) {
                    endDate=new Date(new Date(contentH.substring(ix1+14,ix2+3)));
                    endDate.setMinutes(parseInt(endDate.getMinutes()+iAdd));
                    placeDate=contentH.substring(ix1+14,ix2).replace('T',' mellan klockan ') + ' och ' + ('0'+ endDate.getHours()).slice(-2) + ':' + ('0'+ endDate.getMinutes()).slice(-2);
                } else {
                    placeDate=contentH.substring(ix1+14,ix2).replace('T',' klockan ');
                }
            }
            //Lägger texten i klippbordet
            clipText= 'Eventet äger rum ' + placeDate + ' vid koordinaterna ' + postedCoordinates + ' \nAlla ändringar utöver flytt under 161 meter måste göras av en reviewer.';
            navigator.clipboard.writeText(clipText);

            //Sätter icon på format-raden för att visa att skriptet är kört
            var labelText = document.createElement('span');
            labelText.appendChild(document.createTextNode(' '));
            var page = document.getElementsByClassName("button-type-redo")[0];
            page.appendChild(labelText);

            let img = document.createElement('img');
            img.src = 'data:image/jpg;base64,iVBORw0KGgoAAAANSUhEUgAAACwAAAAuCAYAAABTTPsKAAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAAJcEhZcwAADsMAAA7DAcdvqGQAAAKRSURBVGhD7ZnfS1NhHMbPzsntbKmz5dxSS8gU+o0WRFeFZGA/RFc3Rlddeuf/EFF/QRcRXXoRc828SYQg2vBCCiIIIzGImstmEm7u1/Hk+5z3O0i2NocnPfk+h5eHBw57Xz579933PUcSMlk27jtHY+06s312N+LP9CJcuvkFa5URLKQdQ9gZPsHJ1iMvZZfhQ63X4U/OPBCEq5IS6gRZn9qInMyn4Efr2uHTF4N/rFEQrljBQyB7wNmCmNc1+Nr6xZS4OlN0bYJwWYWOgKzfYexZG19CbDUGl258/uuaBOFS6py8BLIJXl8dih3+LUVk5ytaiyC8USPv7oLsw/lRZA/1CNlf8NsH++GPuu8JwtUp2MbrbTNidi0Hb7DXwecuv9zUGgRhkho+BrLuGoMk1dsF6m8Dc1XNLQhfid4B2UjiDbJLccJj6QW4FKis3paSIExnsmanD3E5Z9TbodZr8Mfd9wXhqkQnB6/Dg0z9rUM2eoavfZEtmWv3Ee6LGFUhuvQW2aWo8Er7281qF+7hscO8KvgR4+kfcG3w45aSJRU+dO/4SUyczCeRFXkPXBsoM/E/XnBhS7BfMxtsYjYUfpWSGj6us+FTmyQ2VrU0xjnPaQyzZN09vH/iLL5aOmvleN9KJ4TZ3incG5gexn1Ti1FmhaoQzxhbQR/8ZMpWIFmXcO34KZCrr6lFJq3wH2GP9zz8Rfw1nJ4yfs8k4PmBWVPJkqxLWA51gLBf9SJvVEbLwmmPU27ivcOH3klBuJgKVNzPu0BY5QQVW/EarOu4zbReoZysS5hUqlqQzP7rLSfrE5aetoBwo8t4UiPzW1Y0491Dqv/9tpAl/QeEuS68ugXS9FZnpufZtpIlWYywJP0GSFvJ25oD99AAAAAASUVORK5CYII=';

            img.height = 21;
            img.width = 18;
            img.title = cacheName;
            page.appendChild(img);

        }
    }
})()

function konvertCoords_toDDM_JS(koordinaterDD) { // Konverterar från DD till DDM
    if (typeof(koordinaterDD) == "undefined") {
        koordinaterDD='';
        return;
    }
    koordinaterDD = koordinaterDD.replace(' ','');
    var ix1 = koordinaterDD.indexOf (',');
    var ix2;
    var latitudDD = koordinaterDD.substring(0,ix1);
    var longitudeDD = koordinaterDD.substring(ix1+1);
    var latitudDMM = latitudDD.substring(0,latitudDD.indexOf('.'));
    var longitudeDMM = longitudeDD.substring(0,longitudeDD.indexOf('.'));
    if (latitudDD.substring(0,1)==='-') {
        latitudDMM = 'S' + latitudDMM.substring(1).padStart(2,'0');
    } else {
        latitudDMM = 'N' + latitudDMM.padStart(2,'0');
    }
    ix2 = latitudDD.substring(latitudDD.indexOf('.')) * 60;
    ix2 = ix2.toFixed(3);
    if (parseInt(ix2) < 10) {
        ix2='0' + ix2;
    }
    latitudDMM += ' ' + ix2;
    if (longitudeDD.substring(0,1)==='-') {
        longitudeDMM = 'W' + longitudeDMM.substring(1).padStart(2,'0');
    } else {
        longitudeDMM = 'E' + longitudeDMM.padStart(2,'0');
    }
    ix2 = longitudeDD.substring(longitudeDD.indexOf('.')) * 60;
    ix2 = ix2.toFixed(3);
    if (parseInt(ix2) < 10) {
        ix2='0' + ix2;
    }
    longitudeDMM += ' ' + ix2;
    return latitudDMM + ' ' + longitudeDMM;
}
/* Inställning i FF för att sätt clipboard
about:config
sök upp dom.events.testing.asyncClipboard
Sätt till true
*/
