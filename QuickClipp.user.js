// ==UserScript==
// @name         QuickClipp
// @namespace    http://tampermonkey.net/
// @version      1.06
// @description  Samla lite eventinformtion, namn, datum, tid, koordinater
// @updateURL    https://gitlab.com/herrFamaxe/src/raw/main/QuickClipp.user.js
// @downloadURL  https://gitlab.com/herrFamaxe/src/raw/main/QuickClipp.user.js
// @author       Mats
// @match        https://www.geocaching.com/live/geocache/*
// @icon         data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw==
// @grant        GM_getValue
// @grant        GM_setValue
// @grant        GM_registerMenuCommand
// @grant        GM_addStyle
// ==/UserScript==

var signedInAs='';
var loggTextArray = new Array(10);
(function() {
    window.onload = function GetEventInfo () {

        var sText;
        var contentH = document.body.innerHTML;
        var gcCode='';
        var cacheName='';
        var cacheOwner='';
        var cacheType='';
        var iAdd=0;
        var postedCoordinates='';
        var startDate;
        var endDate = new Date();
        var placeDate='';
        var eventStartTime='';
        var eventEndTime='';
        var ix1;
        var ix2;

        ix1 = contentH.indexOf('"https://www.geocaching.com/geocache/GC', 0);
        if (ix1>0) {
            // gccode
            ix2 = contentH.indexOf('"', ix1+1);
            gcCode=contentH.substring(ix1+37,ix2);

            // cacheName
            ix1 = contentH.indexOf('cache name link"', 0);
            ix1 = contentH.indexOf('">', ix1);
            ix2 = contentH.indexOf('</', ix1+2);
            cacheName=contentH.substring(ix1+2,ix2);

            //cacheOwner
            ix1 = contentH.indexOf('cache owner link"', 0);
            ix1 = contentH.indexOf('">', ix1);
            ix2 = contentH.indexOf('</', ix1+2);
            cacheOwner=contentH.substring(ix1+2,ix2);

            //cacheType
            ix1 = contentH.indexOf('gc-geocache-icon"><title>', 0);
            ix2 = contentH.indexOf('</', ix1+2);
            cacheType=contentH.substring(ix1+25,ix2);
            //om event, räknar ut sluttid, OBS! bara en estimering
            if (cacheType.indexOf('Event',0) >=0 ) {
                cacheType=cacheType.replace('Event Cache','Event');
                cacheType=cacheType.replace('®','');
                if (cacheType.substr(0,5)=='Event') {iAdd=30;}
                if (cacheType.indexOf('Cache In Trash Out',0) >=0 ) {iAdd=60;}
                if (cacheType.indexOf('Community Celebration', 0) >=0) {iAdd=120;}
            }

            // vem som är påloggad
            ix1 = contentH.indexOf('"username":', ix1);
            if (ix1 > 0) {
                ix2 = contentH.indexOf('"', ix1+12);
                signedInAs=contentH.substring(ix1+12,ix2);
            }

            //postedCoordinates
            ix1 = contentH.indexOf('"postedCoordinates":{"', 0);
            if (ix1 > 0) {
                ix1 = contentH.indexOf('"latitude"', ix1);
                ix2 = contentH.indexOf('}', ix1+1);
                if (ix1 > 0) {
                    postedCoordinates=contentH.substring(ix1+11,ix2).replace('"longitude":','');
                    postedCoordinates=konvertCoords_toDDM_JS(postedCoordinates);
                }
            }

            //placedDate
            ix1 = contentH.indexOf('"placedDate":"20',0);
            ix2 = contentH.indexOf(':00"', ix1+1);
            if (ix1 > 0) {
                placeDate=contentH.substring(ix1+14,ix2-6);
                 eventStartTime = contentH.substr(ix1+25,5);
                if (iAdd > 0) {
                    endDate=new Date(new Date(contentH.substring(ix1+14,ix2+3)));
                    endDate.setMinutes(parseInt(endDate.getMinutes()+iAdd));
                    eventEndTime = ('0'+ endDate.getHours()).slice(-2) + ':' + ('0'+ endDate.getMinutes()).slice(-2);
                }
            }

            loggTextArray[0] = GM_getValue('QuickClippText0_' + signedInAs, 'QuickClippText0.');
            loggTextArray[1] = GM_getValue('QuickClippText1_' + signedInAs, 'QuickClippText1.'); //'Eventet äger rum #eventdatumtid vid koordinaterna #koordinater. |Alla ändringar utöver flytt under 161 meter måste göras av en reviewer.';
            loggTextArray[2] = GM_getValue('QuickClippText2_' + signedInAs, 'QuickClippText2.'); // 'Hej #co||Hoppas att går bra med din nya cache #cachenamn.||Din kompis #signedInAs';
            loggTextArray[3] = GM_getValue('QuickClippText3_' + signedInAs, 'QuickClippText3.'); //'Hälsningar|Kraata-Kal, Geocaching Community Volunteer Reviewer||**Bra länkar**|[Riktlinjer för att gömma en geocache](https://www.geocaching.com/play/guidelines)|[Sveriges avdelning i Geocachings officiella wiki](https://wiki.groundspeak.com/display/GEO/Sweden)|[Help Center](https://www.geocaching.com/help/index.php)|';
            loggTextArray[4] = GM_getValue('QuickClippText4_' + signedInAs, 'QuickClippText4.'); //'Text fyra';
            loggTextArray[5] = GM_getValue('QuickClippText5_' + signedInAs, 'QuickClippText5.');
            loggTextArray[6] = GM_getValue('QuickClippText6_' + signedInAs, 'QuickClippText6.');
            loggTextArray[7] = GM_getValue('QuickClippText7_' + signedInAs, 'QuickClippText7.');
            loggTextArray[8] = GM_getValue('QuickClippText8_' + signedInAs, 'QuickClippText8.');
            loggTextArray[9] = GM_getValue('QuickClippText9_' + signedInAs, 'QuickClippText9.');

            loggTextArray[0] = loggTextArray[0].replaceAll('\n', '|');
            loggTextArray[1] = loggTextArray[1].replaceAll('\n', '|');
            loggTextArray[2] = loggTextArray[2].replaceAll('\n', '|');
            loggTextArray[3] = loggTextArray[3].replaceAll('\n', '|');
            loggTextArray[4] = loggTextArray[4].replaceAll('\n', '|');
            loggTextArray[5] = loggTextArray[5].replaceAll('\n', '|');
            loggTextArray[6] = loggTextArray[6].replaceAll('\n', '|');
            loggTextArray[7] = loggTextArray[7].replaceAll('\n', '|');
            loggTextArray[8] = loggTextArray[8].replaceAll('\n', '|');
            loggTextArray[9] = loggTextArray[9].replaceAll('\n', '|');

            for (var x=0; x <10; x++) {  //TAGGAR #gcCode #cacheName #cacheOwner #cacheType #signedInAs #postedCoordinates #placeDate #eventStartTime #eventEndTime
                loggTextArray[x]=loggTextArray[x].replaceAll(/#gcCode/gi,gcCode);
                loggTextArray[x]=loggTextArray[x].replaceAll(/#cacheName/gi,cacheName);
                loggTextArray[x]=loggTextArray[x].replaceAll(/#cacheOwner/gi,cacheOwner);
                loggTextArray[x]=loggTextArray[x].replaceAll(/#cacheType/gi,cacheType);
                loggTextArray[x]=loggTextArray[x].replaceAll(/#signedInAs/gi,signedInAs);
                loggTextArray[x]=loggTextArray[x].replaceAll(/#postedCoordinates/gi,postedCoordinates);
                loggTextArray[x]=loggTextArray[x].replaceAll(/#placeDate/gi,placeDate);
                loggTextArray[x]=loggTextArray[x].replaceAll(/#eventStartTime/gi,eventStartTime);
                loggTextArray[x]=loggTextArray[x].replaceAll(/#eventEndTime/gi,eventEndTime);
             //   loggTextArray[x]=loggTextArray[x] + '|';
            }
        }

        //Nytt script till sidan som anropas vid keydown i logg-rutan
        var nyDiv = document.createElement('div');
        document.body.appendChild(nyDiv);
        var script = document.createElement("script");
        script.innerHTML  = "function callSkript(e) {";
        script.innerHTML += "    if (e.altKey == true) { ";
        script.innerHTML += "       if (e.key =='0') {getText('" + loggTextArray[0] + "');}";
        script.innerHTML += "       if (e.key =='1') {getText('" + loggTextArray[1] + "');}";
        script.innerHTML += "       if (e.key =='2') {getText('" + loggTextArray[2] + "');}";
        script.innerHTML += "       if (e.key =='3') {getText('" + loggTextArray[3] + "');}";
        script.innerHTML += "       if (e.key =='4') {getText('" + loggTextArray[4] + "');}";
        script.innerHTML += "       if (e.key =='5') {getText('" + loggTextArray[5] + "');}";
        script.innerHTML += "       if (e.key =='6') {getText('" + loggTextArray[6] + "');}";
        script.innerHTML += "       if (e.key =='7') {getText('" + loggTextArray[7] + "');}";
        script.innerHTML += "       if (e.key =='8') {getText('" + loggTextArray[8] + "');}";
        script.innerHTML += "       if (e.key =='9') {getText('" + loggTextArray[9] + "');}";
        script.innerHTML += "    }";
        script.innerHTML += "}";

        script.innerHTML += "function getText(logText) {";
        script.innerHTML += "    var nText = document.getElementById('gc-md-editor_md').value;";
        script.innerHTML += "    var nText2 = logText.replaceAll('|', '\\n');";
        script.innerHTML += "    var nEnd = document.getElementById('gc-md-editor_md').selectionStart + nText2.length;";
        script.innerHTML += "    nText = nText.substring(0,document.getElementById('gc-md-editor_md').selectionStart) + nText2 + nText.substring(document.getElementById('gc-md-editor_md').selectionEnd);";
        script.innerHTML += "    document.getElementById('gc-md-editor_md').value = nText.trim() + '\\n';";
        script.innerHTML += "    document.getElementById('gc-md-editor_md').selectionEnd = nEnd;";
        script.innerHTML += "}";
        document.head.appendChild(script);

        //Nytt event till sidan för att fånga keydown i logg-rutan
        document.getElementById("gc-md-editor_md").addEventListener("keydown", callSkript, false);

        //Sätter icon på format-raden för att visa att skriptet är kört
        var labelText = document.createElement('span');
        labelText.appendChild(document.createTextNode(' '));
        var page = document.getElementsByClassName("button-type-redo")[0];
        page.appendChild(labelText);

        let img = document.createElement('img');
        img.src = 'data:image/jpg;base64,iVBORw0KGgoAAAANSUhEUgAAACwAAAAuCAYAAABTTPsKAAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAAJcEhZcwAADsMAAA7DAcdvqGQAAAEfSURBVGhD7ZY9DoJAEIUXEyOF1h7B1tt4ADsvYbiEvaW3sfUI1tpjduMjZMKEhf2RkfmazTZm+ObxxCiJKb7nZHhdDrU918+7u7+3e3duTjc368LdBDEZw9QsUMOh+JoFatiXoWaBGu5jrFmghjk4s6CoHl6zqGFKn1nf7AI1TKnPO2eYMtQsUMMgtlmghmP1LYc4w9EHtma77NrsIr8hzDfDqVqBMj/Dod+3Q5mfYS67oX3L0fwoXa3vSnMPPDoS9gHxkG1i9S1HMzAtfHqfCs3auNUCrDh3K1DktkSfYRjkYpLqJaOIM+w9MPcSpm4Fyv9mmJKrFShyDQNf07lagSLfcNc/WbsFcmeWIt8waH+JLVelKY/Xn5oFwgwb8wH95JyNn84AUgAAAABJRU5ErkJggg==';

        img.height = 21;
        img.width = 18;
        img.title = cacheName + ', ' + gcCode + ' (Click for QuickClipp settings)';
        img.style.cursor = "pointer";
        img.onclick = function () { showConfig(); }
        page.appendChild(img);

    }
})()

function konvertCoords_toDDM_JS(koordinaterDD) { // Konverterar från DD till DDM
    if (typeof(koordinaterDD) == "undefined") {
        koordinaterDD='';
        return;
    }
    koordinaterDD = koordinaterDD.replace(' ','');
    var ix1 = koordinaterDD.indexOf (',');
    var ix2;
    var latitudDD = koordinaterDD.substring(0,ix1);
    var longitudeDD = koordinaterDD.substring(ix1+1);
    var latitudDMM = latitudDD.substring(0,latitudDD.indexOf('.'));
    var longitudeDMM = longitudeDD.substring(0,longitudeDD.indexOf('.'));
    if (latitudDD.substring(0,1)==='-') {
        latitudDMM = 'S' + latitudDMM.substring(1).padStart(2,'0');
    } else {
        latitudDMM = 'N' + latitudDMM.padStart(2,'0');
    }
    ix2 = latitudDD.substring(latitudDD.indexOf('.')) * 60;
    ix2 = ix2.toFixed(3);
    if (parseInt(ix2) < 10) {
        ix2='0' + ix2;
    }
    latitudDMM += ' ' + ix2;
    if (longitudeDD.substring(0,1)==='-') {
        longitudeDMM = 'W' + longitudeDMM.substring(1).padStart(2,'0');
    } else {
        longitudeDMM = 'E' + longitudeDMM.padStart(2,'0');
    }
    ix2 = longitudeDD.substring(longitudeDD.indexOf('.')) * 60;
    ix2 = ix2.toFixed(3);
    if (parseInt(ix2) < 10) {
        ix2='0' + ix2;
    }
    longitudeDMM += ' ' + ix2;
    return latitudDMM + ', ' + longitudeDMM;
}

// Visa config-bild
function showConfig() {

    createConfigDivHead();
    var divSet = document.getElementById('gm_divSet');
    var nWidth = parseInt(divSet.style.width);
    nWidth=nWidth-100 + 'px';

    // Text-0
    var pBR0 = document.createElement('br');
    var pText0 = document.createElement('p');
    pText0.setAttribute('class', 'SettingElement');

    var labelText0 = document.createElement('label');
    labelText0.setAttribute('for', 'txtText0');
    labelText0.appendChild(document.createTextNode('Alt + 0'));

    var txtText0 = document.createElement('textarea');
    txtText0.id = 'txtText0';
    txtText0.style.marginRight = '6px';
    txtText0.rows='7';
    txtText0.style.width = nWidth;

    var AutoText0 = GM_getValue('QuickClippText0_' + signedInAs, 'QuickClippText0.');
    txtText0.value = AutoText0;

    pText0.appendChild(labelText0);
    pText0.appendChild(pBR0);
    pText0.appendChild(txtText0);
    txtText0.addEventListener('focus', configSelectAllText, true);
    divSet.appendChild(pText0);

    // Text-1
    var pBR1 = document.createElement('br');
    var pText1 = document.createElement('p');
    pText1.setAttribute('class', 'SettingElement');

    var labelText1 = document.createElement('label');
    labelText1.setAttribute('for', 'txtText1');
    labelText1.appendChild(document.createTextNode('Alt + 1'));

    var txtText1 = document.createElement('textarea');
    txtText1.id = 'txtText1';
    txtText1.style.marginRight = '6px';
    txtText1.rows='7';
    txtText1.style.width = nWidth;

    var AutoText1 = GM_getValue('QuickClippText1_' + signedInAs, 'QuickClippText1.');
    txtText1.value = AutoText1;

    pText1.appendChild(labelText1);
    pText1.appendChild(pBR1);
    pText1.appendChild(txtText1);
    txtText1.addEventListener('focus', configSelectAllText, true);
    divSet.appendChild(pText1);

    // Text-2
    var pBR2 = document.createElement('br');
    var pText2 = document.createElement('p');
    pText2.setAttribute('class', 'SettingElement');

    var labelText2 = document.createElement('label');
    labelText2.setAttribute('for', 'txtText2');
    labelText2.appendChild(document.createTextNode('Alt + 2'));

    var txtText2 = document.createElement('textarea');
    txtText2.id = 'txtText2';
    txtText2.style.marginRight = '6px';
    txtText2.rows='7';
    txtText2.style.width = nWidth;

    var AutoText2 = GM_getValue('QuickClippText2_' + signedInAs, 'QuickClippText2.');
    txtText2.value = AutoText2;

    pText2.appendChild(labelText2);
    pText2.appendChild(pBR2);
    pText2.appendChild(txtText2);
    txtText2.addEventListener('focus', configSelectAllText, true);
    divSet.appendChild(pText2);

    // Text-3
    var pBR3 = document.createElement('br');
    var pText3 = document.createElement('p');
    pText3.setAttribute('class', 'SettingElement');

    var labelText3 = document.createElement('label');
    labelText3.setAttribute('for', 'txtText3');
    labelText3.appendChild(document.createTextNode('Alt + 3'));

    var txtText3 = document.createElement('textarea');
    txtText3.id = 'txtText3';
    txtText3.style.marginRight = '6px';
    txtText3.rows='7';
    txtText3.style.width = nWidth;

    var AutoText3 = GM_getValue('QuickClippText3_' + signedInAs, 'QuickClippText3.');
    txtText3.value = AutoText3;

    pText3.appendChild(labelText3);
    pText3.appendChild(pBR3);
    pText3.appendChild(txtText3);
    txtText3.addEventListener('focus', configSelectAllText, true);
    divSet.appendChild(pText3);

    // Text-4
    var pBR4 = document.createElement('br');
    var pText4 = document.createElement('p');
    pText4.setAttribute('class', 'SettingElement');

    var labelText4 = document.createElement('label');
    labelText4.setAttribute('for', 'txtText4');
    labelText4.appendChild(document.createTextNode('Alt + 4'));

    var txtText4 = document.createElement('textarea');
    txtText4.id = 'txtText4';
    txtText4.style.marginRight = '6px';
    txtText4.rows='7';
    txtText4.style.width = nWidth;

    var AutoText4 = GM_getValue('QuickClippText4_' + signedInAs, 'QuickClippText4.');
    txtText4.value = AutoText4;

    pText4.appendChild(labelText4);
    pText4.appendChild(pBR4);
    pText4.appendChild(txtText4);
    txtText4.addEventListener('focus', configSelectAllText, true);
    divSet.appendChild(pText4);

    // Text-5
    var pBR5 = document.createElement('br');
    var pText5 = document.createElement('p');
    pText5.setAttribute('class', 'SettingElement');

    var labelText5 = document.createElement('label');
    labelText5.setAttribute('for', 'txtText5');
    labelText5.appendChild(document.createTextNode('Alt + 5'));

    var txtText5 = document.createElement('textarea');
    txtText5.id = 'txtText5';
    txtText5.style.marginRight = '6px';
    txtText5.rows='7';
    txtText5.style.width = nWidth;

    var AutoText5 = GM_getValue('QuickClippText5_' + signedInAs, 'QuickClippText5.');
    txtText5.value = AutoText5;

    pText5.appendChild(labelText5);
    pText5.appendChild(pBR5);
    pText5.appendChild(txtText5);
    txtText5.addEventListener('focus', configSelectAllText, true);
    divSet.appendChild(pText5);

    // Text-6
    var pBR6 = document.createElement('br');
    var pText6 = document.createElement('p');
    pText6.setAttribute('class', 'SettingElement');

    var labelText6 = document.createElement('label');
    labelText6.setAttribute('for', 'txtText6');
    labelText6.appendChild(document.createTextNode('Alt + 6'));

    var txtText6 = document.createElement('textarea');
    txtText6.id = 'txtText6';
    txtText6.style.marginRight = '6px';
    txtText6.rows='7';
    txtText6.style.width = nWidth;

    var AutoText6 = GM_getValue('QuickClippText6_' + signedInAs, 'QuickClippText6.');
    txtText6.value = AutoText6;

    pText6.appendChild(labelText6);
    pText6.appendChild(pBR6);
    pText6.appendChild(txtText6);
    txtText6.addEventListener('focus', configSelectAllText, true);
    divSet.appendChild(pText6);

    // Text-7
    var pBR7 = document.createElement('br');
    var pText7 = document.createElement('p');
    pText7.setAttribute('class', 'SettingElement');

    var labelText7 = document.createElement('label');
    labelText7.setAttribute('for', 'txtText7');
    labelText7.appendChild(document.createTextNode('Alt + 7'));

    var txtText7 = document.createElement('textarea');
    txtText7.id = 'txtText7';
    txtText7.style.marginRight = '6px';
    txtText7.rows='7';
    txtText7.style.width = nWidth;

    var AutoText7 = GM_getValue('QuickClippText7_' + signedInAs, 'QuickClippText7.');
    txtText7.value = AutoText7;

    pText7.appendChild(labelText7);
    pText7.appendChild(pBR7);
    pText7.appendChild(txtText7);
    txtText7.addEventListener('focus', configSelectAllText, true);
    divSet.appendChild(pText7);

    // Text-8
    var pBR8 = document.createElement('br');
    var pText8 = document.createElement('p');
    pText8.setAttribute('class', 'SettingElement');

    var labelText8 = document.createElement('label');
    labelText8.setAttribute('for', 'txtText8');
    labelText8.appendChild(document.createTextNode('Alt + 8'));

    var txtText8 = document.createElement('textarea');
    txtText8.id = 'txtText8';
    txtText8.style.marginRight = '6px';
    txtText8.rows='7';
    txtText8.style.width = nWidth;

    var AutoText8 = GM_getValue('QuickClippText8_' + signedInAs, 'QuickClippText8.');
    txtText8.value = AutoText8;

    pText8.appendChild(labelText8);
    pText8.appendChild(pBR8);
    pText8.appendChild(txtText8);
    txtText8.addEventListener('focus', configSelectAllText, true);
    divSet.appendChild(pText8);

    // Text-9
    var pBR9 = document.createElement('br');
    var pText9 = document.createElement('p');
    pText9.setAttribute('class', 'SettingElement');

    var labelText9 = document.createElement('label');
    labelText9.setAttribute('for', 'txtText9');
    labelText9.appendChild(document.createTextNode('Alt + 9'));

    var txtText9 = document.createElement('textarea');
    txtText9.id = 'txtText9';
    txtText9.style.marginRight = '6px';
    txtText9.rows='7';
    txtText9.style.width = nWidth;

    var AutoText9 = GM_getValue('QuickClippText9_' + signedInAs, 'QuickClippText9.');
    txtText9.value = AutoText9;

    pText9.appendChild(labelText9);
    pText9.appendChild(pBR9);
    pText9.appendChild(txtText9);
    txtText9.addEventListener('focus', configSelectAllText, true);
    divSet.appendChild(pText9);

    createConfigDivFoot()
}

function createConfigDivHead() {

    var divSet = document.getElementById("gm_divSet");
    if (divSet) {
        var YOffsetVal = parseInt(divSet.getAttribute('YOffsetVal', '0'));
        window.scrollTo(window.pageXOffset, YOffsetVal);
        alert('Edit Setting interface already on screen.');
        return;
    }
    GM_addStyle('.SettingTitle {font-size: medium; font-style: italic; font-weight: bold; ' +
                'text-align: center; margin-bottom: 6px; !important; } ' );
    GM_addStyle('.SettingVersionTitle {font-size: small; font-style: italic; font-weight: bold; ' +
                'text-align: center; margin-bottom: 12px; !important; } ' );
    GM_addStyle('.SettingElement {text-align: left; margin-left: 6px; ' +
                'margin-right: 6px; margin-bottom: 12px; !important; } ' );
    GM_addStyle('.SettingButtons {text-align: right; margin-left: 6px; ' +
                'margin-right: 6px; margin-bottom: 6px; !important; } ' );
    document.body.setAttribute('style', 'height:100%;');

    var popwidth = parseInt(window.innerWidth * .6);
    divSet = document.createElement('div');
    divSet.id = 'gm_divSet';
    divSet.setAttribute('style', 'position: absolute; z-index: 1000; visibility: hidden; padding: 5px; outline: 7px ridge blue; background: #FFFFCC;');
    divSet.style.width = popwidth + 'px';
    divSet.setAttribute('winTopPos', document.documentElement.scrollTop); // snapback code
    divSet.style.visibility = 'visible';

        // Skapa Save/Cancel knappar
    var ds_ButtonsP = document.createElement('div');
    ds_ButtonsP.setAttribute('class', 'SettingButtons');

    var ds_SaveButton = document.createElement('button');
    ds_SaveButton = document.createElement('button');
    ds_SaveButton.appendChild(document.createTextNode("Save"));
    ds_SaveButton.addEventListener("click", configSaveButtonClicked, true);

    var ds_CancelButton = document.createElement('button');
    ds_CancelButton.style.marginLeft = '6px';
    ds_CancelButton.addEventListener("click", configCancelButtonClicked, true);
    ds_CancelButton.appendChild(document.createTextNode("Cancel"));

    ds_ButtonsP.appendChild(ds_SaveButton);
    ds_ButtonsP.appendChild(ds_CancelButton);
    //divSet.appendChild(ds_ButtonsP);

    var ds_Heading = document.createElement('div');
    ds_Heading.setAttribute('class', 'SettingTitle');
    ds_Heading.appendChild(document.createTextNode('QuickClipp settings'));
    ds_Heading.appendChild(ds_ButtonsP);
    divSet.appendChild(ds_Heading);

  //  var ds_Version = document.createElement('div');
  //  ds_Version.setAttribute('class', 'SettingVersionTitle');
  //  ds_Version.appendChild(document.createTextNode('Version ' + GM_info.script.version + ', (' + signedInAs +')'));
  //  divSet.appendChild(ds_Version);

    var toppos = parseInt(window.pageYOffset + 60);
    var leftpos = parseInt((window.innerWidth / 2) - (popwidth / 2));
    divSet.style.top = toppos + 'px';
    divSet.style.left = leftpos + 'px';
    divSet.setAttribute('YOffsetVal', window.pageYOffset);

    document.body.appendChild(divSet);
    //window.addEventListener('resize', fSetLeftPos, true);
}

function createConfigDivFoot() {

    var divSet = document.getElementById("gm_divSet");

 //   GM_addStyle('.SettingTitle {font-size: medium; font-style: italic; font-weight: bold; ' +
  //              'text-align: center; margin-bottom: 6px; !important; } ' );
    GM_addStyle('.SettingVersionTitle {font-size: small; font-style: italic; font-weight: bold; ' +
                'text-align: center; margin-bottom: 12px; !important; } ' );
 //   GM_addStyle('.SettingElement {text-align: left; margin-left: 6px; ' +
 //               'margin-right: 6px; margin-bottom: 12px; !important; } ' );
 //   GM_addStyle('.SettingButtons {text-align: right; margin-left: 6px; ' +
 //               'margin-right: 6px; margin-bottom: 6px; !important; } ' );
  //  document.body.setAttribute('style', 'height:100%;');

    //var popwidth = parseInt(window.innerWidth * .6);

    var ds_Version = document.createElement('div');
    ds_Version.setAttribute('class', 'SettingVersionTitle');
    ds_Version.appendChild(document.createTextNode('Version ' + GM_info.script.version + ', (' + signedInAs +')'));
    divSet.appendChild(ds_Version);

    var ds_tips= document.createElement('div');
    ds_tips.setAttribute('class', 'SettingVersionTitle');
    ds_tips.appendChild(document.createTextNode('ett, två, tre'));
    divSet.appendChild(ds_tips);

    document.body.appendChild(divSet);
    //window.addEventListener('resize', fSetLeftPos, true);
}


function configCancelButtonClicked() {
    configCloseSettingsDiv();
}

function configSetLeftPos() {
    var divSet = document.getElementById('gm_divSet');
    if (divSet) {
        var popwidth = parseInt(window.innerWidth * .5);
        divSet.style.width = popwidth + 'px';
        var leftpos = parseInt((window.innerWidth / 2) - (popwidth / 2));
        divSet.style.left = leftpos + 'px';
    }
}

function configSaveButtonClicked() {
    var txtAutoText0 = document.getElementById('txtText0');
    GM_setValue('QuickClippText0_' + signedInAs, txtAutoText0.value.trim());
    var txtAutoText1 = document.getElementById('txtText1');
    GM_setValue('QuickClippText1_' + signedInAs, txtAutoText1.value.trim());
    var txtAutoText2 = document.getElementById('txtText2');
    GM_setValue('QuickClippText2_' + signedInAs, txtAutoText2.value.trim());
    var txtAutoText3 = document.getElementById('txtText3');
    GM_setValue('QuickClippText3_' + signedInAs, txtAutoText3.value.trim());
    var txtAutoText4 = document.getElementById('txtText4');
    GM_setValue('QuickClippText4_' + signedInAs, txtAutoText4.value.trim());
    var txtAutoText5 = document.getElementById('txtText5');
    GM_setValue('QuickClippText5_' + signedInAs, txtAutoText5.value.trim());
    var txtAutoText6 = document.getElementById('txtText6');
    GM_setValue('QuickClippText6_' + signedInAs, txtAutoText6.value.trim());
    var txtAutoText7 = document.getElementById('txtText7');
    GM_setValue('QuickClippText7_' + signedInAs, txtAutoText7.value.trim());
    var txtAutoText8 = document.getElementById('txtText8');
    GM_setValue('QuickClippText8_' + signedInAs, txtAutoText8.value.trim());
    var txtAutoText9 = document.getElementById('txtText9');
    GM_setValue('QuickClippText9_' + signedInAs, txtAutoText9.value.trim());
    configCloseSettingsDiv();
}

function configCloseSettingsDiv() {
    var divSet = document.getElementById('gm_divSet');
    var winTopPos = divSet.getAttribute('winTopPos') - 0; // snapback code
    removeNode(divSet);
    window.removeEventListener('resize', configSetLeftPos, true);
    document.documentElement.scrollTop = winTopPos; // snapback code
}

function configSelectAllText() {
    //this.select();
}

function removeNode(element) {
    element.parentNode.removeChild(element);
}

// Create menu option to change settings.
GM_registerMenuCommand('QuickClipp settings', showConfig);

/*
TAGG LISTA
#gcCode
#cachenamn
#cacheOwner
#cacheType
#signedInAs
#postedCoordinates
#placeDate
#eventStartTime
#eventEndTime
*/

